using System;
using UnityEngine;

namespace Utils.Attributes
{
	[AttributeUsage(AttributeTargets.Method)]
	public class ButtonAttribute : PropertyAttribute
	{
		public ButtonAttribute()
		{
		}
	}
}