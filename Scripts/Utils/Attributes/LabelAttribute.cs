﻿using UnityEditor;
using UnityEngine;

namespace Utils.Attributes
{
	public class LabelAttribute : PropertyAttribute
	{
		public readonly string NewLabel;
		public LabelAttribute(string newLabel) => NewLabel = newLabel;
	}

#if UNITY_EDITOR
	[CustomPropertyDrawer(typeof(LabelAttribute))]
	public class OverrideLabelDrawer : PropertyDrawer
	{
		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return EditorGUI.GetPropertyHeight(property, label);
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			label.text = ((LabelAttribute) attribute).NewLabel;
			EditorGUI.PropertyField(position, property, label, true);
		}
	}
#endif
}