using System;
using UnityEditor;
using UnityEngine;

namespace Utils.Attributes
{
	[AttributeUsage(AttributeTargets.Field)]
	public class PreviewTextureAttribute : PropertyAttribute
	{
		public int size;
		public PreviewTextureAttribute(int size) => this.size = size;
	}

#if UNITY_EDITOR
	[CustomPropertyDrawer(typeof(PreviewTextureAttribute))]
	public class PreviewTextureDrawer : PropertyDrawer
	{
		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			if (property.objectReferenceValue == null) return EditorGUI.GetPropertyHeight(property, label);
			int size = ((PreviewTextureAttribute) attribute).size;
			return EditorGUI.GetPropertyHeight(property, label) + size;
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			if (property.objectReferenceValue == null)
			{
				EditorGUI.PropertyField(position, property, label, true);
				return;
			}
			int size = ((PreviewTextureAttribute) attribute).size;
			Rect propertyPosition = position;
			propertyPosition.width -= size + 10;
			EditorGUI.PropertyField(propertyPosition, property, label, true);
			position.x = position.x + position.width - size;
			position.width = size;
			position.height = size;
			EditorGUI.DrawPreviewTexture(position, (Texture2D) property.objectReferenceValue);
		}
	}
#endif
}