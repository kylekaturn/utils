using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Utils.Extensions;

namespace Utils.Components.Animations
{
	public class AnimationGenerator : MonoBehaviour
	{
		public List<AnimationClip> animationClips = new List<AnimationClip>();
		public AnimationClip previewAnimationClip;
		public float previewPosition = 0f;
		private void Awake()
		{
			DestroyImmediate(this);
		}
	}

#if UNITY_EDITOR
	[CustomEditor(typeof(AnimationGenerator))]
	public class AnimationGeneratorEditor : Editor
	{
		private AnimationGenerator _animationGenerator;

		private void OnEnable()
		{
			_animationGenerator = target as AnimationGenerator;
		}

		public override void OnInspectorGUI()
		{
			serializedObject.Update();
			
			GUI.enabled = false;
			EditorGUILayout.ObjectField("Script", MonoScript.FromMonoBehaviour((AnimationGenerator)target), typeof(AnimationGenerator), false);
			GUI.enabled = true;

			for (int i = 0; i < _animationGenerator.animationClips.Count; i++)
			{
				EditorGUILayout.BeginHorizontal();
				
				_animationGenerator.animationClips[i] = EditorGUILayout.ObjectField(_animationGenerator.animationClips[i], typeof(AnimationClip), false) as AnimationClip;
				if (GUILayout.Button("Apply"))
				{
					_animationGenerator.animationClips[i].Apply(_animationGenerator.transform);
					EditorUtility.SetDirty(_animationGenerator);
				}
				if (GUILayout.Button("Update"))
				{
					if (EditorUtility.DisplayDialog("AnimationController", "Are you sure to update animation?", "OK", "Cancel"))
					{
						_animationGenerator.animationClips[i].Update(_animationGenerator.transform);
						EditorUtility.SetDirty(_animationGenerator);
					}
				}
				if (GUILayout.Button("Remove"))
				{
					_animationGenerator.animationClips.RemoveAt(i);
					EditorUtility.SetDirty(_animationGenerator);
				}
				EditorGUILayout.EndHorizontal();
			}

			EditorGUILayout.Space(10);
			if (GUILayout.Button("Create by current pose"))
			{
				var animationClip = AnimationClipExtension.Create(_animationGenerator.transform, "Animation");
				_animationGenerator.animationClips.Add(animationClip);
				EditorUtility.SetDirty(_animationGenerator);
			}
			if (GUILayout.Button("Add empty item"))
			{
				_animationGenerator.animationClips.Add(null);
				EditorUtility.SetDirty(_animationGenerator);
			}
			
			EditorGUILayout.Space(10);
			EditorGUILayout.LabelField("Preview");
			EditorGUILayout.PropertyField(serializedObject.FindProperty("previewAnimationClip"));
			if (_animationGenerator.previewAnimationClip != null)
			{
				EditorGUILayout.Slider(serializedObject.FindProperty("previewPosition"), 0f, _animationGenerator.previewAnimationClip.length);
			
				if (AnimationMode.InAnimationMode())
				{
					AnimationMode.BeginSampling();
					AnimationMode.SampleAnimationClip(_animationGenerator.gameObject, _animationGenerator.previewAnimationClip, _animationGenerator.previewPosition);
					AnimationMode.EndSampling();

					if (GUILayout.Button("Stop Preview"))
					{
						AnimationMode.StopAnimationMode();
					}
				}
				else
				{
					if (GUILayout.Button("Start Preview"))
					{
						AnimationMode.StartAnimationMode();
					}
				}
			}
			
			serializedObject.ApplyModifiedProperties();
		}
	}
#endif
}
