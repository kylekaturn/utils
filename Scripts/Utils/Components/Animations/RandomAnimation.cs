using UnityEngine;
using Utils.Extensions;
using Random = UnityEngine.Random;

namespace Utils.Components.Animations
{
	public class RandomAnimation : MonoBehaviour
	{
		private void OnEnable()
		{
			Animator animator = GetComponent<Animator>();
			var a = animator.runtimeAnimatorController.animationClips.GetRandom();
			animator.PlayInFixedTime(a.name, 0, Random.Range(0, 3));
		}
	}
}
