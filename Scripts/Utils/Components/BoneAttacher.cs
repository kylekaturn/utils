using System;
using UnityEngine;

namespace Utils.Components
{
	public class BoneAttacher : MonoBehaviour
	{
		public Animator animator;
		public HumanBodyBones targetBone;

		private void Awake()
		{
			transform.SetParent(animator.GetBoneTransform(targetBone), true);
		}
	}
}