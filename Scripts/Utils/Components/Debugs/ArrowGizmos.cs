using UnityEngine;
using Utils.Extensions;

namespace Utils.Components.Debugs
{
	public class ArrowGizmos : MonoBehaviour
	{
		public bool showSphere = true;
		public float length = 1f;
		public Color color = Color.blue;
		public bool drawOnlySelected = false;

		private void OnDrawGizmos()
		{
			if (!drawOnlySelected) DrawGizmos();
		}

		private void OnDrawGizmosSelected()
		{
			if (drawOnlySelected) DrawGizmos();
		}

		private void DrawGizmos()
		{
			if (showSphere) Gizmos.DrawSphere(transform.position, 0.03f);
			DebugExtensions.DrawArrow(transform.position, transform.position + transform.forward * 0.5f, color);

			//var down = Quaternion.Euler(transform.eulerAngles) * new Vector3(0, -1f, 0);
			//DebugExtensions.DrawArrow(transform.position, transform.position + down * 0.5f, Color.green);
		}
	}
}