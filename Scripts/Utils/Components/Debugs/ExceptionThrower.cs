using System;
using UnityEngine;

namespace Utils.Components.Debugs
{
	public class ExceptionThrower : MonoBehaviour
	{
		private void Start()
		{
			Debug.Log("Hello!");
			int a = 10;
			int b = 5;
			int c = a + b;
			
			
			throw new Exception("Exception! ");
		}
	}
}