using UnityEditor;
using UnityEngine;

namespace Utils.Components.Debugs
{
	[ExecuteAlways]
	public class GameViewUpdater : MonoBehaviour
	{
#if UNITY_EDITOR
		private void OnEnable()
		{
			EditorApplication.update += UpdateGameView;
		}

		private void OnDisable()
		{
			EditorApplication.update -= UpdateGameView;
		}

		private void UpdateGameView()
		{
			UnityEditorInternal.InternalEditorUtility.RepaintAllViews();
		}
#endif
	}
}