using System;
using UnityEngine;

namespace Utils.Components.Debugs
{
	public class GizmosDisplay : MonoBehaviour
	{
		[Range(0f, 0.5f)]
		public float sphereRadius = 0.01f;

		private void OnDrawGizmos()
		{
			Gizmos.DrawWireSphere(transform.position, sphereRadius);
		}
	}
}