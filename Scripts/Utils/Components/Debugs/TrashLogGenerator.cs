using UnityEngine;

namespace Utils.Components.Debugs
{
	public class TrashLogGenerator : MonoBehaviour
	{
		public int count = 10;
		private void Start()
		{
			for (int i = 0; i < count; i++)
			{
				Debug.Log(Random.value * 1000f);
			}
		}
	}
}