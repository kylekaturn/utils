using System;
using UnityEngine;

namespace Utils.Components.EventDispatcher
{
    public class AnimatorEventDispatcher : MonoBehaviour
    {
        public Action<int> OnAnimatorIKCallback;

        private void OnAnimatorIK(int layerIndex)
        {
            OnAnimatorIKCallback?.Invoke(layerIndex);
        }
    }
}
