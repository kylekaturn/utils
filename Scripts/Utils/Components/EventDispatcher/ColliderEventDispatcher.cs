﻿using System;
using UnityEngine;

namespace Utils.Components.EventDispatcher
{
    public class ColliderEventDispatcher : MonoBehaviour
    {
        public Action<Collision> OnCollisionEnterCallback;
        public Action<Collision2D> OnCollisionEnter2DCallback;
        public Action<Collider> OnTriggerEnterCallback;
        public Action<Collider2D> OnTriggerEnter2DCallback;

        private void OnCollisionEnter(Collision collision)
        {
            OnCollisionEnterCallback?.Invoke(collision);
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            OnCollisionEnter2DCallback?.Invoke(collision);
        }

        private void OnTriggerEnter(Collider other)
        {
            OnTriggerEnterCallback?.Invoke(other);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            OnTriggerEnter2DCallback?.Invoke(collision);
        }
    }
}
