using System;
using UnityEngine;

namespace Utils.Components.EventDispatcher
{
    public class MouseEventDispatcher : MonoBehaviour
    {
        public Action OnMouseDownCallback;

        private void OnMouseDown()
        {
            OnMouseDownCallback?.Invoke();
        }
    }
}
