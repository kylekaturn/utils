using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Utils.Components
{
	[ExecuteInEditMode]
	public class MaterialInstancer : MonoBehaviour
	{
		private void OnEnable()
		{
#if UNITY_EDITOR
			if (UnityEditor.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage() != null) return;
#endif

			if (GetComponent<TextMeshProUGUI>() != null)
			{
				TextMeshProUGUI textMeshProUGUI = GetComponent<TextMeshProUGUI>();
				Material clone = Instantiate(textMeshProUGUI.fontMaterial);
				clone.name = clone.name.Replace("(Clone)", "");
				textMeshProUGUI.fontMaterial = clone;
				DestroyImmediate(this);
				return;
			}
			if (GetComponent<Renderer>() != null)
			{
				Renderer rend = GetComponent<Renderer>();
				rend.material = rend.material;
				DestroyImmediate(this);
				return;
			}
			if (GetComponent<Image>() != null)
			{
				Image image = GetComponent<Image>();
				image.material = Instantiate(image.material);
				Debug.Log("[Material Instancer] Material Instancing succeed.");
				DestroyImmediate(this);
			}
		}
	}
}