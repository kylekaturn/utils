using UnityEngine;
using Utils.Internal;

namespace Utils.Components.Particles
{
	public class ParticleLooper : MonoBehaviour
	{
		public void Start()
		{
			ParticleSystem ps = GetComponent<ParticleSystem>();
			DelayedCaller.DelayedCall(1.0f, ps.Play, true, true);
		}
	}
}