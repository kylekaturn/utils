using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utils.Internal;
using Random = UnityEngine.Random;

namespace Utils.Components.Particles
{
	public class ParticlePlayer : MonoBehaviour
	{
		private List<ParticleSystem> _particleSystems;

		private void OnEnable()
		{
			_particleSystems ??= GetComponentsInChildren<ParticleSystem>().ToList();
			_particleSystems.ForEach(x =>
			{
				x.transform.localPosition = Random.insideUnitSphere;
			});
			_particleSystems.ForEach(x => x.Play());
			DelayedCaller.DelayedCall(1.0f, () =>
			{
				_particleSystems.ForEach(x => x.Play());
			}, true, true);
		}

		private void OnDisable()
		{
			DelayedCaller.KillOf(this);
		}
	}
}