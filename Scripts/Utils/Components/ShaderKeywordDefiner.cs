using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderKeywordDefiner : MonoBehaviour
{
	public int num = 0;

	private void OnEnable()
	{
		Debug.Log(num);
		Shader.SetGlobalFloat("_IsHitlineEnabled", num);
	}
}