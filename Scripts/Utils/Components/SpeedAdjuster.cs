using System;
using UnityEngine;

namespace Utils.Components
{
	public class SpeedAdjuster : MonoBehaviour
	{
		[Range(0f, 1f)]
		public float speed = 1.0f;

		private void Update()
		{
			Time.timeScale = speed;
		}
	}
}