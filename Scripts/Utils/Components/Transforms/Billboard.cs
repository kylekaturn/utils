using UnityEngine;
using Utils.Extensions;

namespace Utils.Components.Transforms
{
	[ExecuteAlways]
	public class Billboard : MonoBehaviour
	{
		public static Camera mainCamera;
		public bool updating = true;
		public Transform target;

		private void OnWillRenderObject()
		{
			if (Camera.current == null) return;
			print(Camera.current.name);
			if (Camera.current.name != "SceneCamera") return;
			var pivot = Vector3.ProjectOnPlane(Camera.current.transform.position - transform.parent.position, transform.parent.forward);
			if (!updating) return;
			float angle = Vector3.SignedAngle(transform.parent.up, pivot, transform.parent.forward);
			transform.SetAngleZ(angle);
		}

		private void DrawArrow(Vector3 a, Vector3 b, float arrowheadAngle = 15f, float arrowheadDistance = 1f, float arrowheadLength = 0.1f)
		{
			// Get the Direction of the Vector
			Vector3 dir = b - a;

			// Get the Position of the Arrowhead along the length of the line.
			Vector3 arrowPos = a + (dir * arrowheadDistance);

			// Get the Arrowhead Lines using the direction from earlier multiplied by a vector representing half of the full angle of the arrowhead (y)
			// and -1 for going backwards instead of forwards (z), which is then multiplied by the desired length of the arrowhead lines coming from the point.

			Vector3 up = Quaternion.LookRotation(dir) * new Vector3(0f, Mathf.Sin(arrowheadAngle * Mathf.Deg2Rad), -1f) * arrowheadLength;
			Vector3 down = Quaternion.LookRotation(dir) * new Vector3(0f, -Mathf.Sin(arrowheadAngle * Mathf.Deg2Rad), -1f) * arrowheadLength;
			Vector3 left = Quaternion.LookRotation(dir) * new Vector3(Mathf.Sin(arrowheadAngle * Mathf.Deg2Rad), 0f, -1f) * arrowheadLength;
			Vector3 right = Quaternion.LookRotation(dir) * new Vector3(-Mathf.Sin(arrowheadAngle * Mathf.Deg2Rad), 0f, -1f) * arrowheadLength;

			// Get the End Locations of all points for connecting arrowhead lines.
			Vector3 upPos = arrowPos + up;
			Vector3 downPos = arrowPos + down;
			Vector3 leftPos = arrowPos + left;
			Vector3 rightPos = arrowPos + right;

			// Draw the line from A to B
			Gizmos.DrawLine(a, b);

			// Draw the rays representing the arrowhead.
			Gizmos.DrawRay(arrowPos, up);
			Gizmos.DrawRay(arrowPos, down);
			Gizmos.DrawRay(arrowPos, left);
			Gizmos.DrawRay(arrowPos, right);

			// Draw Connections between rays representing the arrowhead
			Gizmos.DrawLine(upPos, leftPos);
			Gizmos.DrawLine(leftPos, downPos);
			Gizmos.DrawLine(downPos, rightPos);
			Gizmos.DrawLine(rightPos, upPos);
		}
	}
}