﻿using UnityEngine;

namespace Utils.Components.Transforms
{
	public class TransformFollower : MonoBehaviour
	{
		public Transform target;
		private Vector3 _relativePosition;

		private void Awake()
		{
			_relativePosition = transform.position - target.position;
		}

		private void LateUpdate()
		{
			transform.position = target.position + _relativePosition;
		}
	}
}
