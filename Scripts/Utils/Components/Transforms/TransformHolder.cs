using System.Collections.Generic;
using UnityEngine;
using Utils.Internal;

namespace Utils.Components.Transforms
{
	public class TransformHolder : MonoBehaviour
	{
		public List<TransformData> transformDatas;
	}
}
