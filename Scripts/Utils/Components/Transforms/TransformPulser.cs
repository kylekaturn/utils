using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class TransformPulser : MonoBehaviour
{
	void Start()
	{
		transform.DOScale(1.3f, Random.Range(0.4f, 0.6f)).SetEase(Ease.InOutSine).SetLoops(-1, LoopType.Yoyo);
	}
}