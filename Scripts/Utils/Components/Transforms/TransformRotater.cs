﻿using System;
using UnityEditor;
using UnityEngine;

namespace Utils.Components.Transforms
{
	[ExecuteAlways]
	public class TransformRotater : MonoBehaviour
	{
		public bool rotateOnEditor = false;
		public Vector3 rotate;
		[HideInInspector] public Quaternion initialRotation;

#if UNITY_EDITOR
		private void OnEnable()
		{
			if (!rotateOnEditor) return;
			initialRotation = transform.localRotation;
			transform.localRotation = Quaternion.identity;
			EditorApplication.update += UpdateRotation;
		}

		private void OnDisable()
		{
			if (!rotateOnEditor) return;
			EditorApplication.update -= UpdateRotation;
			transform.localRotation = Quaternion.identity;
		}

		void UpdateRotation()
		{
			if (rotateOnEditor && !Application.isPlaying)
			{
				transform.Rotate(rotate * Time.deltaTime);
			}
			else
			{
				transform.localRotation = Quaternion.identity;
			}
		}
#endif

		private void Update()
		{
			if (Application.isPlaying) transform.Rotate(rotate * Time.deltaTime);
		}
	}
}