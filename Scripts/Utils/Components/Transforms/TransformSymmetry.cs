using System;
using UnityEngine;

#if UNITY_EDITOR
namespace Utils.Components.Transforms
{
	[ExecuteAlways]
	public class TransformSymmetry : MonoBehaviour
	{
		public Transform target;

		private void Awake()
		{
			if (Application.isPlaying) Destroy(this);
		}

		private void Update()
		{
			if (Application.isPlaying) return;
			if (target == null) return;

			//Transform
			transform.localPosition = Vector3.Reflect(target.transform.localPosition, Vector3.left);
			transform.localRotation = new Quaternion(
				target.localRotation.x * -1.0f,
				target.localRotation.y,
				target.localRotation.z,
				target.localRotation.w * -1.0f
			);
			transform.localScale = target.localScale;

			//Capsule Collider
			if (target.gameObject.GetComponent<CapsuleCollider>() && gameObject.GetComponent<CapsuleCollider>())
			{
				var a = gameObject.GetComponent<CapsuleCollider>();
				var b = target.gameObject.GetComponent<CapsuleCollider>();
				a.radius = b.radius;
				a.height = b.height;
				a.center = b.center;
			}

			//Sphere Collider
			if (target.gameObject.GetComponent<SphereCollider>() && gameObject.GetComponent<SphereCollider>())
			{
				var a = gameObject.GetComponent<SphereCollider>();
				var b = target.gameObject.GetComponent<SphereCollider>();
				a.radius = b.radius;
				a.center = b.center;
			}

			//Box Collider
			if (target.gameObject.GetComponent<BoxCollider>() && gameObject.GetComponent<BoxCollider>())
			{
				var a = gameObject.GetComponent<BoxCollider>();
				var b = target.gameObject.GetComponent<BoxCollider>();
				a.center = b.center;
				a.size = b.size;
			}
		}
	}
}
#endif