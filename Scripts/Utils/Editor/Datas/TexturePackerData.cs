using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Utils.Attributes;
using Utils.Extensions;

namespace Utils.Editor.Datas
{
	[CreateAssetMenu(fileName = "TexturePackerData.asset", menuName = "Data/TexturePackerData", order = 0)]
	public class TexturePackerData : ScriptableObject
	{
		public enum Channel
		{
			Red,
			Green,
			Blue,
			Alpha,
			Average
		}

		public class SourceData
		{
			public Channel channel;
			public Texture2D texture;
			public Channel useChannel;
			public bool invert;
		}

		[Space(10)]
		[Separator("Red Channel")]
		[PreviewTexture(70)]
		public Texture2D redTexture;
		[Label("Channel Use")] public Channel redUseChannel = Channel.Average;
		[Label("Invert")] public bool redInvert = false;

		[Space(10)]
		[Separator("Green Channel")]
		[PreviewTexture(70)]
		public Texture2D greenTexture;
		[Label("Channel Use")] public Channel greenUseChannel = Channel.Average;
		[Label("Invert")] public bool greenInvert = false;

		[Space(10)]
		[Separator("Blue Channel")]
		[PreviewTexture(70)]
		public Texture2D blueTexture;
		[Label("Channel Use")] public Channel blueUseChannel = Channel.Average;
		[Label("Invert")] public bool blueInvert = false;

		[Space(10)]
		[Separator("Alpha Channel")]
		[PreviewTexture(70)]
		public Texture2D alphaTexture;
		[Label("Channel Use")] public Channel alphaUseChannel = Channel.Average;
		[Label("Invert")] public bool alphaInvert = false;

		[Space(10)]
		[Separator("Output")]
		public Vector2Int resolution = new Vector2Int(2048, 2048);
		public string saveFileName = "ChannelPacked.png";

		[Button]
		public void Save()
		{
			if (redTexture == null && greenTexture == null && blueTexture == null && alphaTexture == null) return;

			var sourceDatas = new List<SourceData>()
			{
				new() {channel = Channel.Red, texture = redTexture, useChannel = redUseChannel, invert = redInvert},
				new() {channel = Channel.Green, texture = greenTexture, useChannel = greenUseChannel, invert = greenInvert},
				new() {channel = Channel.Blue, texture = blueTexture, useChannel = blueUseChannel, invert = blueInvert},
				new() {channel = Channel.Alpha, texture = alphaTexture, useChannel = alphaUseChannel, invert = alphaInvert}
			};

			Color[] finalColors = new Color[resolution.x * resolution.y];
			finalColors.Fill(new Color(0f, 0f, 0f, 0f));

			foreach (var sourceData in sourceDatas)
			{
				if (sourceData.texture == null) continue;
				var clone = sourceData.texture.GetReadableTexture(resolution);
				var colors = clone.GetPixels(0, 0, resolution.x, resolution.y, 0).ToList();
				int index = 0;
				colors.ForEach(color =>
				{
					finalColors[index] = sourceData.channel switch
					{
						Channel.Red => finalColors[index].SetR(GetColorValueByChannel(color, sourceData.useChannel, sourceData.invert)),
						Channel.Green => finalColors[index].SetG(GetColorValueByChannel(color, sourceData.useChannel, sourceData.invert)),
						Channel.Blue => finalColors[index].SetB(GetColorValueByChannel(color, sourceData.useChannel, sourceData.invert)),
						Channel.Alpha => finalColors[index].SetA(GetColorValueByChannel(color, sourceData.useChannel, sourceData.invert)),
						_ => finalColors[index]
					};
					index++;
				});
				DestroyImmediate(clone);
			}

			Texture2D output = new Texture2D(resolution.x, resolution.y, TextureFormat.RGBA32, true);
			output.SetPixels(finalColors);
			output.Apply();
			output.SaveToPNG(AssetDatabase.GetAssetPath(this).RemoveFilenameFromPath() + (saveFileName.Contains(".png") ? saveFileName : saveFileName + ".png"));
			Debug.Log("Textures Packed PNG file saved.");
			AssetDatabase.Refresh();
		}

		private float GetColorValueByChannel(Color color, Channel channel, bool invert = false)
		{
			return channel switch
			{
				Channel.Red => invert ? 1f - color.r : color.r,
				Channel.Green => invert ? 1f - color.g : color.g,
				Channel.Blue => invert ? 1f - color.b : color.b,
				Channel.Alpha => invert ? 1f - color.a : color.a,
				Channel.Average => invert ? (1f - color.r + 1f - color.g + 1f - color.b) / 3f : (color.r + color.g + color.b) / 3f,
				_ => color.r
			};
		}
	}
}