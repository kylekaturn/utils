using UnityEditor;
using UnityEngine;
using Utils.Extensions;

namespace Utils.Editor.Features.Animations
{
	public class AnimationStructureViewer : EditorWindow
	{
		public AnimationClip animationClip;

		[MenuItem("Utils/Animations/", false, 0)]
		[MenuItem("Utils/Animations/Animation Structure Viewer")]
		private static void Init()
		{
			var animationStructureViewer = (AnimationStructureViewer) GetWindow(typeof(AnimationStructureViewer));
			animationStructureViewer.Show();
		}

		private void OnGUI()
		{
			GUILayout.Label("Animation Structure Viewer", EditorStyles.boldLabel);
			animationClip = EditorGUILayout.ObjectField(animationClip, typeof(AnimationClip), false) as AnimationClip;

			if (animationClip == null) return;
			var paths = animationClip.GetPaths();
			paths.ForEach(path =>
			{
				EditorGUILayout.BeginHorizontal();
				GUILayout.Label(path);
				GUILayout.FlexibleSpace();
				if (GUILayout.Button("Delete"))
				{
					if (EditorUtility.DisplayDialog("AnimationStructureViewer", "Are you sure to delete path?", "OK", "Cancel"))
					{
						animationClip = animationClip.DeletePath(path);
					}
				}
				EditorGUILayout.EndHorizontal();
			});

			GUILayout.Space(20);
			GUILayout.Button("Update");
		}
	}
}