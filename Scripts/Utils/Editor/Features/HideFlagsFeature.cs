using UnityEditor;
using UnityEngine;

namespace Utils.Editor.Features
{
	public static class HideFlagsFeature
	{
		[MenuItem("Utils/Show Hiddden GameObjects")]
		private static void ShowHiddenGameObjects()
		{
			var allGameObjects = Object.FindObjectsOfType<GameObject>();
			foreach (var go in allGameObjects)
			{
				switch (go.hideFlags)
				{
					case HideFlags.HideAndDontSave:
						go.hideFlags = HideFlags.DontSave;
						break;
					case HideFlags.HideInHierarchy:
					case HideFlags.HideInInspector:
						go.hideFlags = HideFlags.None;
						break;
				}
			}
		}
	}
}