using UnityEditor;
using UnityEngine;

namespace Utils.Editor.Features.Particles
{
	public static class InitializeParticleFeature
	{
		[MenuItem("CONTEXT/ParticleSystem/InitializeParticle")]
		private static void InitializeParticle(MenuCommand command)
		{
			ParticleSystem particleSystem = command.context as ParticleSystem;
			Debug.Log(particleSystem.name);
		}
	}
}