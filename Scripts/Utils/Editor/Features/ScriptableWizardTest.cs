using UnityEditor;

namespace Utils.Editor.Features
{
	public class ScriptableWizardTest : ScriptableWizard
	{
		public bool CreateAnimationsFolder;
		// Materials
		public bool CreateMaterialsFolder;
		// Prefabs
		public bool CreatePrefabsFolder;
		// Resources
		public bool CreateResourcesFolder;
		// Scripts
		public bool CreateScriptsFolder;
		
		[MenuItem("Utils/ScriptableWizardTest")]
		public static void CreateWizard()
		{
			DisplayWizard<ScriptableWizardTest>("ScriptableWizardTest", "OK");
			
		}

		public void OnWizardCreate()
		{
			
		}

		public void OnWizardUpdate()
		{

		}

		public void OnWizardOtherButton()
		{

		}
	}
}