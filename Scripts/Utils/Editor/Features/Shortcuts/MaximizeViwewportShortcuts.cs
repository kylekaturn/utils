namespace Utils.Editor.Features.ShortCuts
{
	internal static class MaximizeViewportShortcuts
	{
#if UNITY_EDITOR
		[UnityEditor.MenuItem("Utils/MaximizeViewport #A")]
		private static void MaximizeViewport()
		{
			var window = UnityEditor.EditorWindow.focusedWindow;
			if (window == null) return;
			window.maximized = !window.maximized;
		}
#endif
	}
}