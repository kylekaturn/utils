using System;
using UnityEditor;
using UnityEngine;
using Utils.XR;

namespace Utils.Editor.Features.XR
{
	public class LogRenderingConfiguration : EditorWindow
	{
		//[MenuItem("Utils/XR", false, 10)]
		[MenuItem("Utils/XR/Log Rendering Configuration")]
		private static void Init()
		{
			var window = (LogRenderingConfiguration) GetWindow(typeof(LogRenderingConfiguration), true, "Log Rendering Configuration", true);
			window.minSize = new Vector2(370, 350);
			window.maxSize = new Vector2(370, 350);
			window.Show();
		}

		private void OnGUI()
		{
			GUILayout.Space(20);
			XRPlayer xrPlayer = FindObjectOfType<XRPlayer>();
			if (xrPlayer != null)
			{
				GUILayout.Label(GenerateString(xrPlayer.xrConfiguration));
				GUILayout.Space(20);
				if (GUILayout.Button("Copy to Clipboard"))
				{
					GUIUtility.systemCopyBuffer = GenerateString(xrPlayer.xrConfiguration);
					Close();
				}
				if (GUILayout.Button("Copy to Clipboard Horizontal"))
				{
					GUIUtility.systemCopyBuffer = GenerateStringHorizontal(xrPlayer.xrConfiguration);
					Close();
				}
			}
		}

		private string GenerateStringHorizontal(XRConfiguration c) => @$"Unity Version : {Application.unityVersion}
Rendering Mode : {c.renderingMode} : , Depth Priming Mode : {c.depthPrimingMode}, Copy Depth Mode : {c.copyDepthMode}
Use Native Renderpass : {c.useNativeRenderpass}, Renderer Features : {c.rendererFeature}, Depth Texture : {c.depthTexture}
Opaque Texture : {c.opaqueTextue}, Shadow : {c.shadow}, Skybox : {c.skybox}, Post Processing : {c.postProcessing},HDR : {c.hdr}
Bloom : {c.bloom}, Anti-aliasing Type : {c.antialiasingType}, MSAA : {c.msaa}, Render Scale : {c.renderScale}, FFR Level : {c.ffrLevel}";

		private string GenerateString(XRConfiguration c) => @$"Unity Version : {Application.unityVersion}
Rendering Mode : {c.renderingMode}
Depth Priming Mode : {c.depthPrimingMode}
Copy Depth Mode : {c.copyDepthMode}
Use Native Renderpass : {c.useNativeRenderpass}
Renderer Features : {c.rendererFeature}
Depth Texture : {c.depthTexture}
Opaque Texture : {c.opaqueTextue}
Shadow : {c.shadow}
Skybox : {c.skybox}
Post Processing : {c.postProcessing} 
HDR : {c.hdr}
Bloom : {c.bloom}
Anti-aliasing Type : {c.antialiasingType} 
MSAA : {c.msaa}
Render Scale : {c.renderScale}
FFR Level : {c.ffrLevel}";
	}
}