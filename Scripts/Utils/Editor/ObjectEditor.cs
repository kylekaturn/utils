using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using Utils.Attributes;
using Utils.Extensions;
using Utils.Internal;

namespace Utils.Editor
{
	[CustomEditor(typeof(Object), true)]
	public class ObjectEditor : UnityEditor.Editor
	{
		private IEnumerable<MethodInfo> _buttons;

		private void OnEnable()
		{
			_buttons = ReflectionUtility.GetAllMethods(target, m => m.GetCustomAttributes(typeof(ButtonAttribute), true).Length > 0);
		}

		public override void OnInspectorGUI()
		{
			//base.OnInspectorGUI();

			serializedObject.Update();

			List<SerializedProperty> serializedProperties = serializedObject.GetSerializedProperties();

			foreach (var property in serializedProperties)
			{
				//Monoscript Field
				if (property.name.Equals("m_Script", System.StringComparison.Ordinal))
				{
					using (new EditorGUI.DisabledScope(disabled: true)) EditorGUILayout.PropertyField(property);
					continue;
				}

				//Conditional Attribute
				if (property.HasAttribute<ConditionalAttribute>())
				{
					var conditionalAttribute = property.GetAttribute<ConditionalAttribute>();
					bool enabled = ConditionalAttribute.GetConditionalHideAttributeResult(conditionalAttribute, property);
					if (enabled)
					{
						EditorGUILayout.PropertyField(property);
					}
					else
					{
						using (new EditorGUI.DisabledScope(true))
						{
							if (!conditionalAttribute.hideInInspector) EditorGUILayout.PropertyField(property);
						}
					}
					continue;
				}
				EditorGUILayout.PropertyField(property);
			}

			//ButtonMethod
			foreach (MethodInfo button in _buttons)
			{
				if (GUILayout.Button(button.Name)) button.Invoke(serializedObject.targetObject, null);
			}

			serializedObject.ApplyModifiedProperties();
		}
	}
}