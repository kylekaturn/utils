using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEditor.Callbacks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Utils.Editor.Processors
{
	public class BuildProcessor : IPreprocessBuildWithReport
	{
		public int callbackOrder => 0;
		
		public void OnPreprocessBuild(BuildReport report)
		{
			Debug.Log("[BuildProcessor] Preprocess build stared.");

			//빌드세팅에 모든 신을 제거하고, 현재 ActiveScene 을 빌드세팅에 추가함.
			{
				List<EditorBuildSettingsScene> editorBuildSettingsScenes = new List<EditorBuildSettingsScene>();
				editorBuildSettingsScenes.Add(new EditorBuildSettingsScene(SceneManager.GetActiveScene().path, true));
				EditorBuildSettings.scenes = editorBuildSettingsScenes.ToArray();
			}

			//PlayerSettings 의 ProductName 과 Identifier 를 빌드신 이름으로 변경함
			{
				string mainSceneName = SceneUtility.GetScenePathByBuildIndex(0).Split('/').Last().Replace(".unity", "");
				PlayerSettings.productName = mainSceneName;
				PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, "com.lila." + mainSceneName);
			}
		}

		[PostProcessBuild(0)]
		public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
		{
			Debug.Log("[BuildProcessor] Postprocess build stared.");
			//Debug.Log(SceneUtility.GetScenePathByBuildIndex(0));
		}
	}
}