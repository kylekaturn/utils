using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Utils.Editor.Processors
{
	public class FBXImportProcessor : AssetPostprocessor
	{
		private void OnPostprocessModel(GameObject model)
		{
			//Cinema4D 에서 Enemy FBX 익스포트 할때 LOD 메시의 네이밍과 하이라키가 임의로 변경되는것을 조절함
			foreach (Transform x in model.transform)
			{
				if (x.name.Contains("Enemy") && x.name.Contains("_LOD"))
				{
					var skinnedMeshRenderer = x.GetComponentsInChildren<SkinnedMeshRenderer>();
					if (skinnedMeshRenderer.Length > 0)
					{
						skinnedMeshRenderer[0].sharedMesh.name = x.name;
						EditorUtility.CopySerialized(skinnedMeshRenderer[0], x.gameObject.AddComponent<SkinnedMeshRenderer>());
					}
					foreach (Transform y in x.transform)
					{
						Object.DestroyImmediate(y.gameObject);
					}
				}
			}
		}
	}
}