#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Utils.Extensions
{
	public static class AnimationClipExtension
	{
		private class AnimationData
		{
			public string path;
			public Transform transform;
			public Vector3 localPosition;
			public Quaternion localRotation;

			public void Apply()
			{
				transform.localPosition = localPosition;
				transform.localRotation = localRotation;
			}
		}

		//전달된 트랜스폼 기준으로 애니메이션 클립 생성
		public static AnimationClip Create(Transform target, string clipName, bool isLegacy = false)
		{
			AnimationClip animationClip = new AnimationClip();
			animationClip.legacy = isLegacy;
			foreach (Transform t in target.transform.GetChildrenRecursive())
			{
				PutKeyframe(animationClip, t, AnimationUtility.CalculateTransformPath(t, target.transform));
			}
			AssetDatabase.CreateAsset(animationClip, "Assets/" + clipName + ((Random.value * 10000f).ToString("F0")) + ".anim");
			AssetDatabase.Refresh();
			return animationClip;
		}

		//애니메이션클립에 사용된 모든 패스를 반환함
		public static List<string> GetPaths(this AnimationClip animationClip)
		{
			var paths = new List<string>();
			foreach (var curveBinding in AnimationUtility.GetCurveBindings(animationClip))
			{
				if (curveBinding.type != typeof(Transform)) continue;
				var path = curveBinding.path;
				if (!paths.Contains(path)) paths.Add(path);
			}
			return paths;
		}

		//특정패스를 삭제하고 애셋을 Overwrite 함
		public static AnimationClip DeletePath(this AnimationClip animationClip, string path)
		{
			var newAnimationClip = new AnimationClip();
			newAnimationClip.legacy = animationClip.legacy;
			foreach (var binding in AnimationUtility.GetCurveBindings(animationClip))
			{
				if (path == binding.path) continue;
				var curve = AnimationUtility.GetEditorCurve(animationClip, binding);
				newAnimationClip.SetCurve(binding.path, binding.type, binding.propertyName, curve);
			}
			return animationClip.Overwrite(newAnimationClip);
		}

		//전달된 애니메이션클립을 트랜스폼에 적용함
		public static void Apply(this AnimationClip animationClip, Transform target)
		{
			var animationDatas = new List<AnimationData>();

			foreach (var curveBinding in AnimationUtility.GetCurveBindings(animationClip))
			{
				if (curveBinding.type != typeof(Transform)) continue;
				var currentTransform = target.transform.GetChildByPath(curveBinding.path);
				if (currentTransform == null)
				{
					EditorUtility.DisplayDialog("Error", "Transform hierachy doesn't match.", "OK");
					return;
				}

				var animationData = animationDatas.Find(x => x.path == curveBinding.path);
				if (animationData == null)
				{
					animationData = new AnimationData()
					{
						path = curveBinding.path,
						transform = currentTransform,
						localPosition = currentTransform.localPosition,
						localRotation = currentTransform.localRotation
					};
					animationDatas.Add(animationData);
				}

				var animationCurve = AnimationUtility.GetEditorCurve(animationClip, curveBinding);
				var localPosition = currentTransform.localPosition;
				var localRotation = currentTransform.localRotation;

				foreach (var keyframe in animationCurve.keys)
				{
					if (keyframe.time != 0f) continue;
					switch (curveBinding.propertyName)
					{
						case "m_LocalPosition.x":
							animationData.localPosition.x = keyframe.value;
							break;
						case "m_LocalPosition.y":
							animationData.localPosition.y = keyframe.value;
							break;
						case "m_LocalPosition.z":
							animationData.localPosition.z = keyframe.value;
							break;
						case "m_LocalRotation.x":
							animationData.localRotation.x = keyframe.value;
							break;
						case "m_LocalRotation.y":
							animationData.localRotation.y = keyframe.value;
							break;
						case "m_LocalRotation.z":
							animationData.localRotation.z = keyframe.value;
							break;
						case "m_LocalRotation.w":
							animationData.localRotation.w = keyframe.value;
							break;
						case "localEulerAnglesRaw.x":
							//currentTransform.SetLocalAngleX(keyframe.value);
							break;
						case "localEulerAnglesRaw.y":
							//currentTransform.SetLocalAngleY(keyframe.value);
							break;
						case "localEulerAnglesRaw.z":
							//currentTransform.SetLocalAngleZ(keyframe.value);
							break;
					}
				}
			}
			animationDatas.ForEach(x => x.Apply());
		}

		//AnimationClip 애셋을 Overwrite
		public static AnimationClip Overwrite(this AnimationClip animationClip, AnimationClip newAnimationClip)
		{
			newAnimationClip.name = animationClip.name;
			EditorUtility.CopySerialized(newAnimationClip, animationClip);
			AssetDatabase.SaveAssets();
			return animationClip;
		}

		//애니메이션 클립을 overwrite
		public static AnimationClip Update(this AnimationClip animationClip, Transform transform)
		{
			var newAnimationClip = new AnimationClip();
			foreach (Transform t in transform.GetChildrenRecursive())
			{
				PutKeyframe(animationClip, t, AnimationUtility.CalculateTransformPath(t, transform));
			}
			return animationClip.Overwrite(newAnimationClip);
		}

		//Keyframe 생성 및 삽입
		private static void PutKeyframe(this AnimationClip animationClip, Transform t, string path)
		{
			animationClip.SetCurve(path, typeof(Transform), "m_LocalPosition.x", new AnimationCurve(new Keyframe(0.0f, t.localPosition.x)));
			animationClip.SetCurve(path, typeof(Transform), "m_LocalPosition.y", new AnimationCurve(new Keyframe(0.0f, t.localPosition.y)));
			animationClip.SetCurve(path, typeof(Transform), "m_LocalPosition.z", new AnimationCurve(new Keyframe(0.0f, t.localPosition.z)));
			animationClip.SetCurve(path, typeof(Transform), "m_LocalRotation.x", new AnimationCurve(new Keyframe(0.0f, t.localRotation.x)));
			animationClip.SetCurve(path, typeof(Transform), "m_LocalRotation.y", new AnimationCurve(new Keyframe(0.0f, t.localRotation.y)));
			animationClip.SetCurve(path, typeof(Transform), "m_LocalRotation.z", new AnimationCurve(new Keyframe(0.0f, t.localRotation.z)));
			animationClip.SetCurve(path, typeof(Transform), "m_LocalRotation.w", new AnimationCurve(new Keyframe(0.0f, t.localRotation.w)));
		}
	}
}
#endif