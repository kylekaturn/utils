using UnityEngine;

namespace Utils.Extensions
{
	public static class AnimatorExtension
	{
		public static void SetLayerWeightLerp(this Animator animator, int layerIndex, float weight, float t)
		{
			animator.SetLayerWeight(layerIndex, Mathf.Lerp(animator.GetLayerWeight(layerIndex), weight, t));
		}
	}
}