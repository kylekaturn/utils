#if UNITY_EDITOR
using UnityEditor;
using System.Collections.Generic;
using UnityEngine;

namespace Utils.Extensions
{
	public static class AssetExtension
	{
		public static List<T> GetPrefabsHasComponent<T>()
		{
			var ret = new List<T>();

			foreach (string guid in AssetDatabase.FindAssets("t:Prefab"))
			{
				string path = AssetDatabase.GUIDToAssetPath(guid);
				GameObject go = AssetDatabase.LoadAssetAtPath<GameObject>(path);
				if (go.HasComponent<T>()) ret.Add(go.GetComponent<T>());
			}
			return ret;
		}

		public static List<T> GetAssets<T>(string folder = "Assets") where T : UnityEngine.Object
		{
			List<T> ret = new List<T>();
			foreach (string guid in AssetDatabase.FindAssets("t:" + typeof(T).Name, new[] {folder}))
			{
				string path = AssetDatabase.GUIDToAssetPath(guid);
				ret.Add(AssetDatabase.LoadAssetAtPath<T>(path));
			}
			return ret;
		}
	}
}
#endif