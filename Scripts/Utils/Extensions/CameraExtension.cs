using UnityEngine;
using DG.Tweening;

namespace Utils.Extensions
{
	public static class CameraExtension
	{
		public static Tweener DoBackgroundColor(this Camera target, Color endValue, float duration)
		{
			return DOTween.To(() => target.backgroundColor, (x) => target.backgroundColor = x, endValue, duration).SetTarget(target);
		}
	}
}