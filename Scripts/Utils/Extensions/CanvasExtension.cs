using UnityEngine;

namespace Utils.Extensions
{
	public static class CanvasExtension
	{
		public static Vector3 GetHitPositionByCoord(this Canvas canvas, Vector2 coord)
		{
			RectTransform rectTransform = canvas.transform as RectTransform;
			if (rectTransform == null) return Vector3.zero;
			Vector2 a = (coord + new Vector2(-0.5f, -0.5f)) * new Vector2(rectTransform.rect.width, rectTransform.rect.height);
			return rectTransform.TransformPoint(a);
		}
	}
}