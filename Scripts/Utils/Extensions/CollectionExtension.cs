using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Utils.Extensions
{
	public static class CollectionExtension
	{
		//같은 값으로 컬렉션 채움
		public static void Fill<T>(this IList<T> collection, T value)
		{
			for (int i = 0; i < collection.Count; i++)
			{
				collection[i] = value;
			}
		}

		public static bool IsNullOrEmpty<T>(this T[] collection) => collection == null || collection.Length == 0;
		public static bool IsNullOrEmpty<T>(this IList<T> collection) => collection == null || collection.Count == 0;

		//public static bool IsNullOrEmpty<T>(this IEnumerable<T> collection) => collection == null || !collection.Any();
		public static bool NotNullOrEmpty<T>(this T[] collection) => !collection.IsNullOrEmpty();
		public static bool NotNullOrEmpty<T>(this IList<T> collection) => !collection.IsNullOrEmpty();
		//public static bool NotNullOrEmpty<T>(this IEnumerable<T> collection) => !collection.IsNullOrEmpty();

		public static int IndexOfItem<T>(this IEnumerable<T> collection, T item)
		{
			if (collection == null)
			{
				Debug.LogError("IndexOfItem Caused: source collection is null");
				return -1;
			}
			var index = 0;
			foreach (var i in collection)
			{
				if (Equals(i, item)) return index;
				++index;
			}
			return -1;
		}

		public static T[] InsertAt<T>(this T[] array, int index)
		{
			if (index < 0)
			{
				Debug.LogError("Index is less than zero. Array is not modified");
				return array;
			}

			if (index > array.Length)
			{
				Debug.LogError("Index exceeds array length. Array is not modified");
				return array;
			}

			T[] newArray = new T[array.Length + 1];
			int index1 = 0;
			for (int index2 = 0; index2 < newArray.Length; ++index2)
			{
				if (index2 == index) continue;

				newArray[index2] = array[index1];
				++index1;
			}
			return newArray;
		}

		public static T[] RemoveAt<T>(this T[] array, int index)
		{
			if (index < 0)
			{
				Debug.LogError("Index is less than zero. Array is not modified");
				return array;
			}

			if (index >= array.Length)
			{
				Debug.LogError("Index exceeds array length. Array is not modified");
				return array;
			}

			T[] newArray = new T[array.Length - 1];
			int index1 = 0;
			for (int index2 = 0; index2 < array.Length; ++index2)
			{
				if (index2 == index) continue;

				newArray[index1] = array[index2];
				++index1;
			}
			return newArray;
		}

		//Random
		public static T GetRandom<T>(this T[] collection) => collection[Random.Range(0, collection.Length)];
		public static T GetRandom<T>(this IList<T> collection) => collection[Random.Range(0, collection.Count)];
		public static T GetRandom<T>(this IEnumerable<T> collection) => collection.ElementAt(Random.Range(0, collection.Count()));

		//Random Except
		public static T GetRandomExcept<T>(this IList<T> collection, T exception)
		{
			int index = 0;
			while (index++ < 100)
			{
				var item = collection[Random.Range(0, collection.Count)];
				if (item.Equals(exception)) continue;
				return item;
			}
			return collection[0];
		}

		//Pop Elements
		public static T PopAt<T>(this List<T> list, int index)
		{
			var ret = list[index];
			list.Remove(ret);
			return ret;
		}

		public static T PopFirst<T>(this List<T> list) => list.PopAt(0);
		public static T PopLast<T>(this List<T> list) => list.PopAt(list.Count - 1);
		public static T PopRandom<T>(this List<T> list) => list.PopAt(Random.Range(0, list.Count));
		public static T GetLast<T>(this List<T> list) => list[^1];

		public static List<T> RemoveLast<T>(this List<T> list)
		{
			list.RemoveAt(list.Count - 1);
			return list;
		}
	}
}