﻿using UnityEngine;

namespace Utils.Extensions
{
	public static class ColorExtension
	{
		public static string ToHexString(this Color color)
		{
			return $"#{ColorUtility.ToHtmlStringRGB(color)}";
		}
		
		public static Color ToColor(this string hex)
		{
			if (!hex.Contains("#")) hex = "#" + hex;
			byte r = byte.Parse(hex.Substring(1, 2), System.Globalization.NumberStyles.HexNumber);
			byte g = byte.Parse(hex.Substring(3, 2), System.Globalization.NumberStyles.HexNumber);
			byte b = byte.Parse(hex.Substring(5, 2), System.Globalization.NumberStyles.HexNumber);
			byte a = (hex.Length < 8) ? byte.Parse("FF", System.Globalization.NumberStyles.HexNumber) : byte.Parse(hex.Substring(7, 2), System.Globalization.NumberStyles.HexNumber);
			return new Color(r, g, b, a);
		}

		public static Color ToRGB(this Color color)
		{
			return new Color(Mathf.Pow(color.r, 0.45f), Mathf.Pow(color.g, 0.45f), Mathf.Pow(color.b, 0.45f), color.a);
		}

		public static Color ToLinear(this Color color)
		{
			return new Color(Mathf.Pow(color.r, 2.2f), Mathf.Pow(color.g, 2.2f), Mathf.Pow(color.b, 2.2f), color.a);
		}

		public static Color32 SetR(this Color32 color, byte value)
		{
			return new Color32(value, color.g, color.b, color.a);
		}

		public static Color32 SetG(this Color32 color, byte value)
		{
			return new Color32(color.r, value, color.b, color.a);
		}

		public static Color32 SetB(this Color32 color, byte value)
		{
			return new Color32(color.r, color.g, value, color.a);
		}

		public static Color32 SetA(this Color32 color, byte value)
		{
			return new Color32(color.r, color.g, color.b, value);
		}

		public static Color SetR(this Color color, float value)
		{
			return new Color(value, color.g, color.b, color.a);
		}

		public static Color SetG(this Color color, float value)
		{
			return new Color(color.r, value, color.b, color.a);
		}

		public static Color SetB(this Color color, float value)
		{
			return new Color(color.r, color.g, value, color.a);
		}

		public static Color SetA(this Color color, float value)
		{
			return new Color(color.r, color.g, color.b, value);
		}
	}
}