﻿using System;
using System.Collections;
using JetBrains.Annotations;
using UnityEngine;

namespace Utils.Extensions
{
	public static class CoroutineUtils
	{
		public static IEnumerator Then([NotNull] this IEnumerator predecessor, [NotNull] IEnumerator successor)
		{
			while (predecessor.MoveNext())
			{
				yield return predecessor.Current;
			}
			while (successor.MoveNext())
			{
				yield return successor.Current;
			}
		}

		public static IEnumerator Then([NotNull] this IEnumerator predecessor, [NotNull] Action successor)
		{
			while (predecessor.MoveNext())
			{
				yield return predecessor.Current;
			}
			successor.Invoke();
		}

		public static IEnumerator Then([NotNull] this IEnumerator predecessor, [CanBeNull] YieldInstruction successor)
		{
			while (predecessor.MoveNext())
			{
				yield return predecessor.Current;
			}
			yield return successor;
		}

		public static IEnumerator Repeat([NotNull] this IEnumerator predecessor, float interval, int count, [CanBeNull] Action<int> successor)
		{
			while (predecessor.MoveNext())
			{
				yield return predecessor.Current;
			}

			//Debug.Assert(count>0);
			for (int i = 0; i < count; i++)
			{
				yield return new WaitForSeconds(interval);
				if (successor != null) successor.Invoke(i + 1);
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="predecessor"></param>
		/// <param name="successor"></param>
		/// <returns></returns>
		/// <example>
		/// 
		/// StartCoroutine(
		///     new System.Action(createFX.Play)
		///		.Then( new WaitForSeconds(createFX.duration ) )
		///		.Then(ShootOnce )
		///		.Then( new WaitForSeconds( 1.5f ) )
		///		.Then(Shoot )
		///	);
		/// </example>
		public static IEnumerator Then([NotNull] this Action predecessor, [NotNull] IEnumerator successor)
		{
			predecessor.Invoke();
			while (successor.MoveNext())
			{
				yield return successor.Current;
			}
		}

		public static Action Then([NotNull] this Action predecessor, [NotNull] Action successor)
		{
			return delegate
			{
				predecessor.Invoke();
				successor.Invoke();
			};
		}

		public static IEnumerator Then([NotNull] this Action predecessor, [CanBeNull] YieldInstruction successor)
		{
			predecessor.Invoke();
			yield return successor;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="predecessor"></param>
		/// <param name="interval"></param>
		/// <param name="count"></param>
		/// <param name="successor"></param>
		/// <returns></returns>
		/// <example>
		/// 
		/// StartCoroutine(new WaitForSeconds(0.2f)
		///         .Then(()=>
		///        { Debug.Log("Start"); })
		///        .Repeat(0.3f,4,(count)=>{
		///            Debug.Log(count);
		///        }).Then(new WaitForSeconds(2.0f)).Then(()=>{
		///            Debug.Log("End");
		///        }));
		/// </example>
		public static IEnumerator Repeat([NotNull] this Action predecessor, float interval, int count, [CanBeNull] Action<int> successor)
		{
			predecessor.Invoke();

			//Debug.Assert(count>0);
			for (int i = 0; i < count; i++)
			{
				yield return new WaitForSeconds(interval);
				if (successor != null) successor.Invoke(i + 1);
			}
			if (count == 0) yield return null;
		}

		public static IEnumerator Then([CanBeNull] this YieldInstruction predecessor, [NotNull] IEnumerator successor)
		{
			yield return predecessor;
			while (successor.MoveNext())
			{
				yield return successor.Current;
			}
		}

		public static IEnumerator Then([CanBeNull] this YieldInstruction predecessor, [NotNull] Action successor)
		{
			yield return predecessor;
			successor.Invoke();
		}

		public static IEnumerator Then([CanBeNull] this YieldInstruction predecessor, [CanBeNull] YieldInstruction successor)
		{
			yield return predecessor;
			yield return successor;
		}


		public static IEnumerator Repeat([NotNull] this YieldInstruction predecessor, float interval, int count, [CanBeNull] Action<int> successor)
		{
			yield return predecessor;

			Debug.Assert(count > 0);
			for (int i = 0; i < count; i++)
			{
				yield return new WaitForSeconds(interval);
				successor?.Invoke(i + 1);
			}
		}

		public static IEnumerator ThenCoroutine([NotNull] this IEnumerator predecessor, [NotNull] MonoBehaviour owner, [NotNull] IEnumerator coroutine)
		{
			while (predecessor.MoveNext())
			{
				yield return predecessor.Current;
			}
			yield return owner.StartCoroutine(coroutine);
		}

		public static IEnumerator ThenCoroutine([CanBeNull] this YieldInstruction predecessor, [NotNull] MonoBehaviour owner, [NotNull] IEnumerator coroutine)
		{
			yield return predecessor;
			yield return owner.StartCoroutine(coroutine);
		}
	}
}