using UnityEngine;

namespace Utils.Extensions
{
	public static class DebugExtensions
	{
		public static void DrawPivot(Vector3 position, Vector3 eulerAngle)
		{
			var forward = Quaternion.Euler(eulerAngle) * Vector3.forward;
			DrawArrow(position, position + forward.normalized * 0.1f, Color.blue, 15f, 1f, 0.05f);
			var up = Quaternion.Euler(eulerAngle) * Vector3.up;
			DrawArrow(position, position + up.normalized * 0.1f, Color.green, 15f, 1f, 0.05f);
			var right = Quaternion.Euler(eulerAngle) * Vector3.right;
			DrawArrow(position, position + right.normalized * 0.1f, Color.red, 15f, 1f, 0.05f);
		}

		public static void DrawArrow(Vector3 from, Vector3 to, Color color, float arrowheadAngle = 15f, float arrowheadDistance = 1f, float arrowheadLength = 0.1f)
		{
			Vector3 dir = to - from;
			Vector3 arrowPos = from + (dir * arrowheadDistance);
			Vector3 up = Quaternion.LookRotation(dir) * new Vector3(0f, Mathf.Sin(arrowheadAngle * Mathf.Deg2Rad), -1f) * arrowheadLength;
			Vector3 down = Quaternion.LookRotation(dir) * new Vector3(0f, -Mathf.Sin(arrowheadAngle * Mathf.Deg2Rad), -1f) * arrowheadLength;
			Vector3 left = Quaternion.LookRotation(dir) * new Vector3(Mathf.Sin(arrowheadAngle * Mathf.Deg2Rad), 0f, -1f) * arrowheadLength;
			Vector3 right = Quaternion.LookRotation(dir) * new Vector3(-Mathf.Sin(arrowheadAngle * Mathf.Deg2Rad), 0f, -1f) * arrowheadLength;
			Vector3 upPos = arrowPos + up;
			Vector3 downPos = arrowPos + down;
			Vector3 leftPos = arrowPos + left;
			Vector3 rightPos = arrowPos + right;

			Gizmos.color = color;
			//중심라인
			Gizmos.DrawLine(from, to);

			//Arrow헤드
			Gizmos.DrawRay(arrowPos, up);
			Gizmos.DrawRay(arrowPos, down);
			Gizmos.DrawRay(arrowPos, left);
			Gizmos.DrawRay(arrowPos, right);

			//애로우헤드 연결
			Gizmos.DrawLine(upPos, leftPos);
			Gizmos.DrawLine(leftPos, downPos);
			Gizmos.DrawLine(downPos, rightPos);
			Gizmos.DrawLine(rightPos, upPos);
		}
	}
}