using UnityEngine;

namespace Utils.Extensions
{
	public static class LODGroupExtension
	{
		public static int GetCurrentLOD(this LODGroup target)
		{
			Transform transform = target.transform;
			foreach (Transform child in transform.GetChildren())
			{
				var renderer = child.GetComponent<Renderer>();
				if (renderer != null && renderer.isVisible)
				{
					return transform.GetChildren().IndexOf(child);
				}
			}
			return 0;
		}
	}
}