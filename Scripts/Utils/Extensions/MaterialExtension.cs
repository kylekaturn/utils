using UnityEngine;
using DG.Tweening;

namespace Utils.Extensions
{
	public static class MaterialExtension
	{
		
		public static Tweener DOEmissiveColor(this Material target, Color endValue, float duration)
		{
			return DOTween.To(() => target.GetColor("_EmissionColor"), (x) => target.SetColor("_EmissionColor", x), endValue, duration).SetTarget(target);
		}

		public static Texture2D BakeTexture2D(this Material material, int resolution, bool hasMipmap, bool linear)
		{
			resolution = Mathf.Clamp(resolution, 4, SystemInfo.maxTextureSize);
			linear = (QualitySettings.activeColorSpace == ColorSpace.Linear && linear) ? true : false;

			RenderTextureReadWrite renderTextureReadWrite = linear ? RenderTextureReadWrite.Linear : RenderTextureReadWrite.Default;
			RenderTexture renderTexture = RenderTexture.GetTemporary(resolution, resolution, 16, RenderTextureFormat.Default, renderTextureReadWrite);
			material.UpdateRenderTexture(ref renderTexture);
			RenderTexture previousRT = RenderTexture.active;
			RenderTexture.active = renderTexture;

			Texture2D texture = new Texture2D(resolution, resolution, TextureFormat.ARGB32, hasMipmap, linear);
			texture.ReadPixels(new Rect(0, 0, resolution, resolution), 0, 0, hasMipmap);
			texture.Apply(hasMipmap);

			RenderTexture.ReleaseTemporary(renderTexture);
			RenderTexture.active = previousRT;

			return texture;
		}

		public static void UpdateRenderTexture(this Material material, ref RenderTexture renderTexture)
		{
			if (renderTexture == null)
			{
				Debug.LogWarning("Cannot update RenderTexture, it is null.\n");
				return;
			}

			RenderTexture previousRT = RenderTexture.active;
			RenderTexture.active = renderTexture;

			GL.PushMatrix();
			GL.LoadProjectionMatrix(Matrix4x4.Ortho(-0.5f, 0.5f, -0.5f, 0.5f, -100, 100));
			GL.Clear(true, true, Color.clear);
			if (material.SetPass(0)) Graphics.DrawMeshNow(Resources.GetBuiltinResource(typeof(Mesh), "Quad.fbx") as Mesh, Matrix4x4.identity);
			GL.PopMatrix();

			RenderTexture.active = previousRT;
		}
	}
}