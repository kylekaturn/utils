﻿using System;
using UnityEngine;

namespace Utils.Extensions
{
	public static class MathExtension
	{
		public static float Remap(this float value, float from1, float to1, float from2, float to2)
		{
			return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
		}

		public static float GetAngle(Vector2 a, Vector2 b)
		{
			return Mathf.Atan2(b.y - a.y, b.x - a.x) * 180f / Mathf.PI;
		}

		public static Vector2 RadianToVector2(float radian)
		{
			return new Vector2(Mathf.Cos(radian), Mathf.Sin(radian));
		}

		public static Vector2 RadianToVector2(float radian, float length)
		{
			return RadianToVector2(radian) * length;
		}

		public static Vector2 DegreeToVector2(float degree)
		{
			return RadianToVector2(degree * Mathf.Deg2Rad);
		}

		public static Vector2 DegreeToVector2(float degree, float length)
		{
			return RadianToVector2(degree * Mathf.Deg2Rad) * length;
		}

		public static Vector3 ToVector3(this float value)
		{
			return new Vector3(value, value, value);
		}

		public static float ToFloat(this bool value)
		{
			return Convert.ToSingle(value);
		}

		public static float ToOdd(this bool odd)
		{
			return odd ? 1f : -1f;
		}

		public static bool ToBool(this float value)
		{
			return Convert.ToBoolean(value);
		}

		public static int ToInt(this float value)
		{
			int ret;
			try
			{
				ret = Convert.ToInt32(value);
			}
			catch (Exception)
			{
				ret = 0;
			}
			return ret;
		}

		public static int ToInt(this string str)
		{
			return Convert.ToInt32(str);
		}

		public static bool IsFloat(this string str)
		{
			return float.TryParse(str, out float temp);
		}

		public static int FloorTen(this float value)
		{
			float ret = value / 10f;
			return Mathf.FloorToInt(ret) * 10;
		}

		public static int FloorHundred(this float value)
		{
			float ret = value / 100f;
			return Mathf.FloorToInt(ret) * 100;
		}

		public static float FormatDecimal(this float value, uint decimalPoint)
		{
			string str = value.ToString("0.00");
			return float.Parse(str);
		}
	}
}