﻿using UnityEngine;
using UnityEngine.Rendering;

namespace Utils.Extensions
{
	public static class ParticleExtension
	{
		public static void Burst(this ParticleSystem particleSystem, int amount)
		{
			ParticleSystem.Burst burst = new ParticleSystem.Burst
			{
				time = 0.00f,
				minCount = (short) amount,
				maxCount = (short) amount,
				cycleCount = 1,
				repeatInterval = 0.01f
			};

			ParticleSystem.Burst[] bursts = new ParticleSystem.Burst[] {burst};
			particleSystem.emission.SetBursts(bursts);
			particleSystem.Emit(1);
		}

		public static void EnableEmission(this ParticleSystem particleSystem, bool enabled)
		{
			var module = particleSystem.emission;
			module.enabled = enabled;
		}

		public static void SetEmissionRateOverTime(this ParticleSystem particleSystem, int rate)
		{
			var module = particleSystem.emission;
			module.rateOverTime = rate;
		}

		public static void SetLoop(this ParticleSystem particleSystem, bool loop)
		{
			var module = particleSystem.main;
			module.loop = loop;
		}

		public static void SetGravity(this ParticleSystem particleSystem, float gravity)
		{
			var module = particleSystem.main;
			module.gravityModifier = gravity;
		}

		public static void SetStartColor(this ParticleSystem particleSystem, Color color, bool includeChildren = false)
		{
			var module = particleSystem.main;
			module.startColor = color;
			if (includeChildren)
			{
				particleSystem.transform.GetChildren().ForEach((x) =>
				{
					x.GetComponent<ParticleSystem>()?.SetStartColor(color);
				});
			}
		}

		public static void SetStartSpeed(this ParticleSystem particleSystem, float speed)
		{
			var module = particleSystem.main;
			module.startSpeed = speed;
		}

		public static void SetStartLifeTime(this ParticleSystem particleSystem, float startLifeTime)
		{
			var module = particleSystem.main;
			module.startLifetime = startLifeTime;
		}

		public static void SetRateOverTime(this ParticleSystem particleSystem, float rateOverTime)
		{
			var module = particleSystem.emission;
			module.rateOverTime = rateOverTime;
		}

		public static void SetStartSpeedMultiplier(this ParticleSystem particleSystem, float multiplier)
		{
			var module = particleSystem.main;
			module.startSpeedMultiplier = multiplier;
		}

		public static void SetStopAction(this ParticleSystem particleSystem, ParticleSystemStopAction stopAction)
		{
			var module = particleSystem.main;
			module.stopAction = stopAction;
		}

		public static void SetMaxParticles(this ParticleSystem particleSystem, int maxParticles)
		{
			var module = particleSystem.main;
			module.maxParticles = maxParticles;
		}

		public static void SetShapeType(this ParticleSystem particleSystem, ParticleSystemShapeType type)
		{
			var module = particleSystem.shape;
			module.shapeType = type;
		}

		public static void SetSimulationSpace(this ParticleSystem particleSystem, ParticleSystemSimulationSpace simulationSpace)
		{
			var module = particleSystem.main;
			module.simulationSpace = simulationSpace;
		}

		public static void SetStartSize(this ParticleSystem particleSystem, float startSize)
		{
			var module = particleSystem.main;
			module.startSize = startSize;
		}
		
		public static void SetVelocityOverLifetime(this ParticleSystem particleSystem, Vector3 velocity)
		{
			var module = particleSystem.velocityOverLifetime;
			module.x = velocity.x;
			module.y = velocity.y;
			module.z = velocity.z;
		}
	}
}