using UnityEngine;

namespace Utils.Extensions
{
    public static class PhysicsExtension
    {
        public static bool Raycast(Vector3 origin, Vector3 direction, float distance, string mask, out RaycastHit raycastHit)
        {
            return Physics.Raycast(origin, direction, out raycastHit, distance, LayerMask.GetMask(mask));
        }
    }
}

