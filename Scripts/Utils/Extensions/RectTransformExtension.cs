using UnityEngine;

namespace Utils.Extensions
{
    public static class RectTransformExtension 
    {
        public static Vector3 GetHitPositionByCoord(this RectTransform rectTransform, Vector2 coord)
        {
            Vector2 a = (coord + new Vector2(-0.5f, -0.5f)) * new Vector2(rectTransform.rect.width, rectTransform.rect.height);
            return rectTransform.TransformPoint(a);
        }
    }
}
