﻿using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;

namespace Utils.Extensions
{
	public static class TransformExtension
	{
		public static Vector3 GetFloorFoward(this Transform transform)
		{
			return Vector3.ProjectOnPlane(transform.forward, Vector3.up).normalized;
		}

		public static void Shake(this Transform transform, float strength = 1f, float frquency = 10f, float duration = 1f)
		{
			DOVirtual.Float(1f, 0f, duration, (x) =>
			{
				transform.SetLocalX(Mathf.Sin(Time.time * frquency) * x * strength);
			}).OnComplete(() =>
			{
				transform.SetLocalX(0);
			});
		}

		public static void Reset(this Transform transform, bool isLocally = false)
		{
			if (isLocally)
			{
				transform.localPosition = Vector3.zero;
				transform.localEulerAngles = Vector3.zero;
			}
			else
			{
				transform.position = Vector3.zero;
				transform.eulerAngles = Vector3.zero;
			}
		}

		public static void DestroyChildren(this Transform transform)
		{
			while (transform.childCount > 0)
			{
				Object.DestroyImmediate(transform.GetChild(0).gameObject);
			}
		}

		public static int GetChildrenCount(this Transform transform)
		{
			return transform.GetChildren().Count;
		}

		public static List<Transform> GetChildren(this Transform transform)
		{
			List<Transform> ret = new List<Transform>();
			for (int i = 0; i < transform.childCount; i++)
			{
				ret.Add(transform.GetChild(i));
			}
			return ret;
		}

		public static List<Transform> GetChildrenRecursive(this Transform transform)
		{
			List<Transform> ret = new List<Transform>();
			foreach (var child in transform.GetChildren())
			{
				ret.Add(child);
				ret.AddRange(child.GetChildrenRecursive());
			}
			return ret;
		}

		public static Transform GetChildByName(this Transform transform, string name)
		{
			return transform.GetChildren().FirstOrDefault(child => child.name == name);
		}

		public static Transform GetChildByPath(this Transform tranform, string path)
		{
			Transform ret = tranform;
			foreach (var str in path.Split("/"))
			{
				ret = ret.GetChildByName(str);
				if (ret == null) return null;
			}
			return ret;
		}

		#region Look Functions

		public static Quaternion GetLookRotation(this Transform transform, Vector3 target)
		{
			return Quaternion.LookRotation(target - transform.position);
		}

		public static Quaternion GetLookRotation(this Transform transform, Transform target)
		{
			return transform.GetLookRotation(target.position);
		}

		public static void LookAtInverse(this Transform transform, Transform target)
		{
			Quaternion toRotation = Quaternion.LookRotation(transform.position - target.position);
			transform.rotation = toRotation;
		}

		public static void LookAtYOnly(this Transform transform, Vector3 target)
		{
			Vector3 relativePosition = target - transform.position;
			relativePosition.y = 0;
			transform.rotation = Quaternion.LookRotation(relativePosition);
		}

		public static void LookAtYOnly(this Transform transform, Transform target)
		{
			transform.LookAtYOnly(target.position);
		}

		public static void LookAtLerp(this Transform transform, Vector3 target, float amount)
		{
			Quaternion fromRotation = transform.rotation;
			Quaternion toRotation = Quaternion.LookRotation(target - transform.position);
			transform.rotation = Quaternion.Lerp(fromRotation, toRotation, amount);
		}

		#endregion

		#region Set Functions

		public static void SetX(this Transform transform, float value)
		{
			transform.position = new Vector3(value, transform.position.y, transform.position.z);
		}

		public static void SetY(this Transform transform, float value)
		{
			transform.position = new Vector3(transform.position.x, value, transform.position.z);
		}

		public static void SetZ(this Transform transform, float value)
		{
			transform.position = new Vector3(transform.position.x, transform.position.y, value);
		}

		public static void SetXLerp(this Transform transform, float value, float lerp)
		{
			transform.position = new Vector3(Mathf.Lerp(transform.position.x, value, lerp), transform.position.y, transform.position.z);
		}

		public static void SetYLerp(this Transform transform, float value, float lerp)
		{
			transform.position = new Vector3(transform.position.x, Mathf.Lerp(transform.position.y, value, lerp), transform.position.z);
		}

		public static void SetZLerp(this Transform transform, float value, float lerp)
		{
			transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(transform.position.z, value, lerp));
		}

		public static void SetLocalX(this Transform transform, float value)
		{
			transform.localPosition = new Vector3(value, transform.localPosition.y, transform.localPosition.z);
		}

		public static void SetLocalY(this Transform transform, float value)
		{
			transform.localPosition = new Vector3(transform.localPosition.x, value, transform.localPosition.z);
		}

		public static void SetLocalZ(this Transform transform, float value)
		{
			transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, value);
		}

		public static void SetLocalXLerp(this Transform transform, float value, float lerp)
		{
			transform.localPosition = new Vector3(Mathf.Lerp(transform.localPosition.x, value, lerp), transform.localPosition.y, transform.localPosition.z);
		}

		public static void SetLocalYLerp(this Transform transform, float value, float lerp)
		{
			transform.localPosition = new Vector3(transform.localPosition.x, Mathf.Lerp(transform.localPosition.y, value, lerp), transform.localPosition.z);
		}

		public static void SetLocalZLerp(this Transform transform, float value, float lerp)
		{
			transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, Mathf.Lerp(transform.localPosition.z, value, lerp));
		}

		public static void SetAngleX(this Transform transform, float value)
		{
			transform.eulerAngles = new Vector3(value, transform.eulerAngles.y, transform.eulerAngles.z);
		}

		public static void SetAngleY(this Transform transform, float value)
		{
			transform.eulerAngles = new Vector3(transform.eulerAngles.x, value, transform.eulerAngles.z);
		}

		public static void SetAngleZ(this Transform transform, float value)
		{
			transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, value);
		}

		public static void SetLocalAngleX(this Transform transform, float value)
		{
			transform.localEulerAngles = new Vector3(value, transform.localEulerAngles.y, transform.localEulerAngles.z);
		}

		public static void SetLocalAngleY(this Transform transform, float value)
		{
			transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, value, transform.localEulerAngles.z);
		}

		public static void SetLocalAngleZ(this Transform transform, float value)
		{
			transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, value);
		}

		public static void SetLocalRotationX(this Transform transform, float value)
		{
			transform.localRotation = new Quaternion(value, transform.localRotation.y, transform.localRotation.z, transform.localRotation.w);
		}

		public static void SetLocalRotationY(this Transform transform, float value)
		{
			transform.localRotation = new Quaternion(transform.localRotation.x, value, transform.localRotation.z, transform.localRotation.w);
		}

		public static void SetLocalRotationZ(this Transform transform, float value)
		{
			transform.localRotation = new Quaternion(transform.localRotation.x, transform.localRotation.y, value, transform.localRotation.w);
		}

		public static void SetLocalRotationW(this Transform transform, float value)
		{
			transform.localRotation = new Quaternion(transform.localRotation.x, transform.localRotation.y, transform.localRotation.z, value);
		}

		#endregion

		#region Add Functions

		public static void AddX(this Transform transform, float value)
		{
			transform.SetX(transform.position.x + value);
		}

		public static void AddY(this Transform transform, float value)
		{
			transform.SetY(transform.position.y + value);
		}

		public static void AddZ(this Transform transform, float value)
		{
			transform.SetZ(transform.position.z + value);
		}

		public static void AddLocalX(this Transform transform, float value)
		{
			transform.SetLocalX(transform.localPosition.x + value);
		}

		public static void AddLocalY(this Transform transform, float value)
		{
			transform.SetLocalY(transform.localPosition.y + value);
		}

		public static void AddLocalZ(this Transform transform, float value)
		{
			transform.SetLocalZ(transform.localPosition.z + value);
		}

		public static void SetScaleX(this Transform transform, float value)
		{
			transform.localScale = new Vector3(value, transform.localScale.y, transform.localScale.z);
		}

		public static void SetScaleY(this Transform transform, float value)
		{
			transform.localScale = new Vector3(transform.localScale.x, value, transform.localScale.z);
		}

		public static void SetScaleZ(this Transform transform, float value)
		{
			transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, value);
		}

		#endregion
	}
}