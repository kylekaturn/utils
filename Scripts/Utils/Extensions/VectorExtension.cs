﻿using UnityEngine;

namespace Utils.Extensions
{
	public static class VectorExtension
	{
		public static Vector3 GetUpDirection(this Vector3 direction)
		{
			return Quaternion.LookRotation(direction) * new Vector3(0f, 1f, 0f);
		}

		public static Vector3 GetDownDirection(this Vector3 direction)
		{
			return Quaternion.LookRotation(direction) * new Vector3(0f, -1f, 0);
		}
		
		public static Vector3 GetDownRightDirection(this Vector3 direction)
		{
			return Quaternion.LookRotation(direction) * new Vector3(0.5f, -1f, 0);
		}

		public static Vector3 GetLeftDirection(this Vector3 direction)
		{
			return Quaternion.LookRotation(direction) * new Vector3(1f, 0f, 0f);
		}

		public static Vector3 GetRightDirection(this Vector3 direction)
		{
			return Quaternion.LookRotation(direction) * new Vector3(-1f, 0f, 0f);
		}

		public static Vector2 Rotate(this Vector2 vector2, float delta)
		{
			delta *= Mathf.Deg2Rad;
			return new Vector2(
				vector2.x * Mathf.Cos(delta) - vector2.y * Mathf.Sin(delta),
				vector2.x * Mathf.Sin(delta) + vector2.y * Mathf.Cos(delta)
			);
		}

		public static Vector3 Floor(this Vector3 vector3)
		{
			return new Vector3(vector3.x, 0, vector3.z);
		}

		//Roate world
		//vector = Quaternion.Euler(0, -45, 0) * vector;
		
		public static Vector2 ToVector2(this Vector3 vector3)
		{
			return new Vector2(vector3.x, vector3.y);
		}

		public static Vector3 ToVector3(this Vector2 vector2)
		{
			return new Vector3(vector2.x, vector2.y, 0);
		}

		public static Vector3 Multiply(this Vector3 vector3, Vector3 multiply)
		{
			return new Vector3(vector3.x * multiply.x, vector3.y * multiply.y, vector3.z * multiply.z);
		}

		public static Vector3 GetPositionByAngle(float degrees, float distance = 1f)
		{
			float radians = degrees * Mathf.Deg2Rad;
			float x = Mathf.Cos(radians);
			float y = Mathf.Sin(radians);
			return new Vector3(x, y, 0) * distance;
		}
	}
}