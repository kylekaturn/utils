using UnityEditor;
using System;
using UnityEngine;
using Utils.Extensions;

namespace Utils.Internal
{
	[Serializable]
	public class ButtonMethod
	{
		public string label;
		public Action action;

		public ButtonMethod(string label, Action action)
		{
			this.label = label;
			this.action = action;
		}
	}

#if UNITY_EDITOR
	[CustomPropertyDrawer(typeof(ButtonMethod))]
	public class MethodButtonAttributeDrawer : PropertyDrawer
	{
		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return 20;
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			string propertyName = property.name;
			ButtonMethod buttonMethod = property.GetValue() as ButtonMethod;
			
			if (GUI.Button(position, buttonMethod?.label))
			{
				Action action = property.GetValue() as Action;
				buttonMethod?.action?.Invoke();
			}
		}
	}
#endif
}