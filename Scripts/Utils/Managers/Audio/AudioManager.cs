﻿using UnityEditor;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using UnityEngine;
using Utils.Extensions;
using Utils.Internal;
using Random = UnityEngine.Random;

namespace Utils.Managers.Audio
{
	public class AudioManager : MonoBehaviour
	{
		public static AudioManager Instance { get; private set; }

		public int maxStreams = 2;
		public float intervalBetweenAudios = 100f;

		public List<AudioClip> audioClips;
		private EnumeratorList<AudioSource> _audioSources = new EnumeratorList<AudioSource>();
		private Dictionary<AudioClip, Stopwatch> _stopWatches = new Dictionary<AudioClip, Stopwatch>();

		private float _volume;

		public float volume
		{
			get => _volume;
			set
			{
				_volume = value;
				_audioSources.ForEach(x => x.volume = value);
			}
		}

		private void Awake()
		{
			Instance = this;
		}

		private void Start()
		{
			for (int i = 0; i < maxStreams; i++)
			{
				GameObject go = new GameObject
				{
					name = "AudioSource" + i,
					transform =
					{
						parent = transform
					}
				};

				AudioSource audioSource = go.AddComponent<AudioSource>();
				audioSource.playOnAwake = false;
				audioSource.loop = false;
				audioSource.maxDistance = 50;
				audioSource.minDistance = 1;
				//audioSource.rolloffMode = AudioRolloffMode.Linear;
				_audioSources.Add(audioSource);
			}
			volume = 1f;
		}

		public void StopAllSound()
		{
			_audioSources.ForEach(x => x.Stop());
		}

		public void StopSound(string soundName)
		{
			if (soundName == "") return;
			foreach (AudioSource s in _audioSources)
			{
				if (s.clip == null) continue;
				if (s.clip.name == soundName) s.Stop();
			}
		}

		public void PlayAudioRandomly(string audioName, float volume = 1, float pitch = 1, bool preventDuplicate = true, bool spatialize = false, Vector3 position = default)
		{
			PlayAudio(GetAudioClipByName(audioName), Random.Range(volume - 0.1f, volume + 0.1f), Random.Range(pitch - 0.1f, pitch + 0.1f), preventDuplicate, spatialize, position);
		}

		public void PlayAudio(AudioList audioName, float volume = 1, float pitch = 1, bool preventDuplicate = true, bool spatialize = false, Vector3 position = default)
		{
			PlayAudio(GetAudioClipByName(audioName.ToString()), volume, pitch, preventDuplicate, spatialize, position);
		}

		public void PlayAudio(string audioName, float volume = 1, float pitch = 1, bool preventDuplicate = true, bool spatialize = false, Vector3 position = default)
		{
			PlayAudio(GetAudioClipByName(audioName), volume, pitch, preventDuplicate, spatialize, position);
		}

		public void PlayAudio(AudioClip audioClip, float volume = 1, float pitch = 1, bool preventDuplicate = true, bool spatialize = false, Vector3 position = default)
		{
			AudioSource audioSource = _audioSources.next;

			if (preventDuplicate)
			{
				if (!_stopWatches.ContainsKey(audioClip))
				{
					Stopwatch sw = new Stopwatch();
					_stopWatches[audioClip] = sw;
					sw.Start();
				}
				else if (_stopWatches[audioClip].ElapsedMilliseconds < intervalBetweenAudios)
				{
					return;
				}

				_stopWatches[audioClip].Reset();
				_stopWatches[audioClip].Start();

				try
				{
					audioSource = _audioSources.First(x => x.clip == audioClip);
				}
				catch
				{
				}
			}

			audioSource.clip = audioClip;
			audioSource.volume = volume;
			audioSource.pitch = pitch;
			audioSource.spatialize = spatialize;
			audioSource.transform.position = position;
			audioSource.spatialBlend = spatialize ? 1.0f : 0f;
			audioSource.time = 0f;
			audioSource.Play();
		}

		private AudioClip GetAudioClipByName(string audioClipName)
		{
			return audioClips.First(x => x.name == audioClipName);
		}
	}

#if UNITY_EDITOR
	[CustomEditor(typeof(AudioManager))]
	public class SoundManagerEditor : Editor
	{
		private AudioManager _audioManager;
		private SerializedProperty _maxStreams;
		private SerializedProperty _intervalBetweenSounds;
		private SerializedProperty _audioClips;

		private void OnEnable()
		{
			_audioManager = target as AudioManager;
			_maxStreams = serializedObject.FindProperty("maxStreams");
			_intervalBetweenSounds = serializedObject.FindProperty("intervalBetweenSounds");
			_audioClips = serializedObject.FindProperty("audioClips");
		}

		public override void OnInspectorGUI()
		{
			serializedObject.Update();

			// EditorGUI.BeginDisabledGroup(true);
			// EditorGUILayout.ObjectField("Script", MonoScript.FromMonoBehaviour((SoundManager) target), typeof(SoundManager), false);
			// EditorGUI.EndDisabledGroup();
			//
			// EditorGUILayout.Space(10);
			//
			// EditorGUILayout.PropertyField(maxStreams);
			// EditorGUILayout.PropertyField(intervalBetweenSounds);
			//
			// EditorGUILayout.Space(10);
			//
			// EditorGUILayout.LabelField("Audio Clips", EditorStyles.boldLabel);
			//
			// EditorGUI.BeginDisabledGroup(true);
			// IEnumerator enumerator = audioClips.GetEnumerator();
			// while (enumerator.MoveNext())
			// {
			// 	EditorGUILayout.PropertyField(enumerator.Current as SerializedProperty);
			// }
			// EditorGUI.EndDisabledGroup();
			//
			// EditorGUILayout.Space(10);

			DrawDefaultInspector();

			if (GUILayout.Button("Organize AudioClips")) OrganizeSoundClips();
			if (GUILayout.Button("Create Enum Class")) UpdateEnumClass();

			serializedObject.ApplyModifiedProperties();
		}

		public void OrganizeSoundClips()
		{
			_audioManager.audioClips = AssetExtension.GetAssets<AudioClip>("Assets/Audios");
			EditorUtility.SetDirty(_audioManager);
		}

		public void UpdateEnumClass()
		{
			string code = @"
namespace Utils.Managers.Audio{
	public enum AudioList{";
			for (int i = 0; i < _audioManager.audioClips.Count; i++)
			{
				code += _audioManager.audioClips[i].name;
				if (i < _audioManager.audioClips.Count - 1) code += ",";
				code += "\n";
			}
			code += @"
		}
}";
			File.WriteAllText("Assets/Scripts/Utils/Managers/Audio/AudioList.cs", code);
			AssetDatabase.Refresh();
		}
	}
#endif
}