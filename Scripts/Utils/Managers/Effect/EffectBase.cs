﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Utils.Internal;
using Utils.Managers.Audio;
using static UnityEngine.ParticleSystem;

namespace Utils.Managers.Effect
{
	public class EffectBase : MonoBehaviour
	{
		public enum EffectGenerateMethod { Singleton, Pool, Clone }
		public enum EffectBehaviour { None, MoveToTarget }

		public EffectGenerateMethod effectGenerateMethod = EffectGenerateMethod.Clone;
		public EffectBehaviour effectBehavoiur = EffectBehaviour.None;
		new ParticleSystem particleSystem;
		[HideInInspector]
		public int instanceAmount = 10;
		[HideInInspector]
		public bool playAudio = false;
		[HideInInspector]
		public AudioClip audioClip;
		[HideInInspector]
		public float audioDelay = 0f;
		[HideInInspector]
		public bool preventAudioDuplicate = true;

		private List<ParticleSystem> _particleSystems;
		private MainModule _mainModule;
		private Vector3 _target;
		private float _targetLerp;
		private Vector3 _initialScale;

		private void Awake()
		{
			_initialScale = transform.localScale;
			particleSystem = GetComponent<ParticleSystem>();
			_particleSystems = new List<ParticleSystem> {particleSystem};
			_mainModule = particleSystem.main;

			_mainModule.stopAction = ParticleSystemStopAction.Disable;

			for (int i = 0; i < transform.childCount; i++)
			{
				ParticleSystem child = transform.GetChild(i).GetComponent<ParticleSystem>();
				if (child != null) _particleSystems.Add(child);
			}
			gameObject.SetActive(false);
		}

		private void Start()
		{
			switch (effectGenerateMethod)
			{
				case EffectGenerateMethod.Singleton:
					break;
				case EffectGenerateMethod.Clone:
					break;
				case EffectGenerateMethod.Pool:
					break;
			}
		}

		private void Update()
		{
			switch (effectBehavoiur)
			{
				case EffectBehaviour.None:
					break;
				case EffectBehaviour.MoveToTarget:
					Particle[] particles = new Particle[particleSystem.particleCount];
					int count = particleSystem.GetParticles(particles);

					for (int i = 0; i < count; i++)
					{
						particles[i].position = Vector3.Lerp(particles[i].position, _target, _targetLerp);
					}
					particleSystem.SetParticles(particles, count);
					break;
			}
		}

		public void Play(int count = -1)
		{
			gameObject.SetActive(true);
			effectBehavoiur = EffectBehaviour.None;
			if (count == -1)
			{
				particleSystem.Play();
			}
			else
			{
				particleSystem.Emit(count - 1);
				for (int i = 1; i < _particleSystems.Count; i++)
				{
					_particleSystems.ForEach((x) => x.Play());
				}
			}
			PlaySound();
		}

		private void PlaySound()
		{
			if (playAudio && AudioManager.Instance != null)
			{
				DelayedCaller.DelayedCall(audioDelay, () =>
				{
					AudioManager.Instance.PlayAudio(audioClip, 1, 1, preventAudioDuplicate);
				});
			}
		}

		public void MoveToTarget(Vector3 target, float duration)
		{
			this._target = target;
			effectBehavoiur = EffectBehaviour.MoveToTarget;
			_targetLerp = 0f;
			DOTween.To(() => _targetLerp, (x) => _targetLerp = x, 1, duration).SetEase(Ease.InSine);
		}

		public EffectBase Clone()
		{
			EffectBase ret = Instantiate(gameObject).GetComponent<EffectBase>();
			ret.transform.position = new Vector2(9999, 9999);
			return ret;
		}

		public float scale
		{
			set { transform.localScale = _initialScale * value; }
		}

		public Color color
		{
			set
			{
				foreach (ParticleSystem ps in _particleSystems)
				{
					MainModule mm = ps.main;
					mm.startColor = value;
				}
			}
		}
	}
}