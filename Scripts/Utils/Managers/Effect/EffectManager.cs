﻿using UnityEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using Utils.Extensions;
using Utils.Internal;

namespace Utils.Managers.Effect
{
	[Serializable]
	public class EffectData
	{
		public ParticleSystem effect;
		public int instanceAmount = 10;
		[NonSerialized] public EnumeratorList<ParticleSystem> instances;
	}

	public class EffectManager : MonoBehaviour
	{
		public static EffectManager Instance { get; private set; }
		public List<EffectData> EffectDatas = new List<EffectData>();

		private void Awake()
		{
			Instance = this;
			GenerateEffectInstances();
		}

		private void GenerateEffectInstances()
		{
			transform.GetChildren().ForEach(x => DestroyImmediate(x.gameObject));

			EffectDatas.ForEach(effectData =>
			{
				effectData.instances = new EnumeratorList<ParticleSystem>();

				for (int i = 0; i < effectData.instanceAmount; i++)
				{
					var instance = Instantiate(effectData.effect, transform, true);
					instance.gameObject.SetActive(false);
					instance.SetStopAction(ParticleSystemStopAction.Disable);
					effectData.instances.Add(instance);
				}
			});
		}

		public ParticleSystem PlayEffect(EffectBase effect, Vector3 position, Vector3 rotation = default, float scale = 1, int count = -1, Color? color = null)
		{
			return PlayEffect(effect.gameObject.name, position, rotation, scale, count, color);
		}

		public ParticleSystem PlayEffect(EffectList effectName, Vector3 position, Vector3 rotation = default, float scale = 1, int count = -1, Color? color = null)
		{
			return PlayEffect(effectName.ToString(), position, rotation, scale, count, color);
		}

		public ParticleSystem PlayEffect(string effectName, Vector3 position, Vector3 rotation = default, float scale = 1, int count = -1, Color? color = null)
		{
			ParticleSystem ps = GetEffectByName(effectName);
			ps.gameObject.SetActive(true);
			ps.transform.position = position;
			ps.transform.eulerAngles = rotation;
			//ps.gameObject.transform.localScale = new Vector3(scale, scale, scale);
			ps.Play();
			return ps;
		}

		private ParticleSystem GetEffectByName(string effectName)
		{
			return EffectDatas.First(effectData => effectData.effect.name == effectName).instances.next;
		}
	}

#if UNITY_EDITOR
	[CustomEditor(typeof(EffectManager))]
	public class EffectManagerEditor : Editor
	{
		private EffectManager _effectManager;

		private void OnEnable()
		{
			_effectManager = target as EffectManager;
		}

		public override void OnInspectorGUI()
		{
			serializedObject.Update();

			DrawDefaultInspector();

			if (GUILayout.Button("Create Enum Class")) UpdateEnumClass();

			serializedObject.ApplyModifiedProperties();
		}

		public void UpdateEnumClass()
		{
			string code = @"
namespace Utils.Managers.Effect{
	public enum EffectList{";
			for (int i = 0; i < _effectManager.EffectDatas.Count; i++)
			{
				code += _effectManager.EffectDatas[i].effect.name;
				if (i < _effectManager.EffectDatas.Count - 1) code += ",";
				code += "\n";
			}
			code += @"
		}
}";
			File.WriteAllText("Assets/Scripts/Utils/Managers/Effect/EffectList.cs", code);
			AssetDatabase.Refresh();
		}
	}
#endif
}