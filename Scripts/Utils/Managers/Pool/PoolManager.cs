﻿using System;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using Utils.Extensions;
using Utils.Internal;

namespace Utils.Managers.Pool
{
	[Serializable]
	public class PoolData
	{
		public PoolObject poolObject;
		public int instanceAmount = 10;
		[NonSerialized] public EnumeratorList<PoolObject> instances;
	}

	public class PoolManager : MonoBehaviour
	{
		public static PoolManager Instance { get; private set; }
		public List<PoolData> poolDatas = new List<PoolData>();


		private void Awake()
		{
			Instance = this;
			foreach (PoolData poolData in poolDatas)
			{
				poolData.instances = new EnumeratorList<PoolObject>();

				for (int i = 0; i < poolData.instanceAmount; i++)
				{
					PoolObject instance = poolData.poolObject.Clone();
					instance.transform.parent = transform;
					instance.transform.position = new Vector3(9999, 9999, 9999);
					instance.Hide();
					poolData.instances.Add(instance);
				}
			}
		}

		public PoolObject PutObject(PoolObject obj, Vector3 position, Vector3 rotation = default)
		{
			return PutObject(obj.gameObject.name, position, rotation);
		}

		public PoolObject PutObject(PoolList objectName, Vector3 position, Vector3 rotation = default)
		{
			return PutObject(objectName.ToString(), position, rotation);
		}

		public PoolObject PutObject(string objectName, Vector3 position, Vector3 rotation = default)
		{
			PoolObject poolInstance = GetObject(objectName);
			poolInstance.transform.position = position;
			poolInstance.transform.eulerAngles = rotation;
			poolInstance.gameObject.SetActive(true);
			poolInstance.Show();
			return poolInstance;
		}

		public PoolObject GetObject(PoolObject obj)
		{
			return GetObject(obj.gameObject.name);
		}

		public PoolObject GetObject(string objectName)
		{
			return poolDatas.First(x => x.poolObject.gameObject.name == objectName).instances.next;
		}
	}

#if UNITY_EDITOR
	[CustomEditor(typeof(PoolManager))]
	public class PoolManagerEditor : Editor
	{
		private PoolManager _poolManager;

		private void OnEnable()
		{
			_poolManager = target as PoolManager;
		}

		public override void OnInspectorGUI()
		{
			serializedObject.Update();

			// EditorGUI.BeginDisabledGroup(true);
			// EditorGUILayout.ObjectField("Script", MonoScript.FromMonoBehaviour((PoolManager) target), typeof(PoolManager), false);
			// EditorGUI.EndDisabledGroup();
			//
			// EditorGUILayout.Space(10);
			//
			// EditorGUILayout.LabelField("Pool Objects", EditorStyles.boldLabel);
			//
			// poolManager.poolDatas.ForEach(poolData =>
			// {
			// 	EditorGUILayout.BeginHorizontal();
			// 	EditorGUI.BeginDisabledGroup(true);
			// 	EditorGUILayout.ObjectField(poolData.poolObject, typeof(PoolObject), false);
			// 	EditorGUI.EndDisabledGroup();
			// 	GUILayout.FlexibleSpace();
			// 	EditorGUILayout.LabelField("Instance amount", GUILayout.Width(100));
			// 	EditorGUI.BeginChangeCheck();
			// 	poolData.instanceAmount = EditorGUILayout.IntField(poolData.instanceAmount, GUILayout.Width(50));
			// 	if (EditorGUI.EndChangeCheck()) EditorUtility.SetDirty(poolManager);
			// 	EditorGUILayout.EndHorizontal();
			// });
			//
			// EditorGUILayout.Space(10);

			DrawDefaultInspector();

			if (GUILayout.Button("Organize Pool Objects")) OrganizePoolObjects();
			if (GUILayout.Button("Create Enum class")) UpdateEnumClass();

			serializedObject.ApplyModifiedProperties();
		}

		private void OrganizePoolObjects()
		{
			List<PoolData> poolDatas = new List<PoolData>();
			AssetExtension.GetPrefabsHasComponent<PoolObject>().ForEach(x =>
			{
				var found = _poolManager.poolDatas.FirstOrDefault(y => y.poolObject == x);
				poolDatas.Add(new PoolData()
				{
					poolObject = x,
					instanceAmount = found?.instanceAmount ?? 10
				});
			});
			_poolManager.poolDatas = poolDatas;
			EditorUtility.SetDirty(_poolManager);
		}

		public void UpdateEnumClass()
		{
			string code = @"
namespace Utils.Managers.Pool{
	public enum PoolList{";
			for (int i = 0; i < _poolManager.poolDatas.Count; i++)
			{
				code += _poolManager.poolDatas[i].poolObject.name;
				if (i < _poolManager.poolDatas.Count - 1) code += ",";
				code += "\n";
			}
			code += @"
		}
}";
			File.WriteAllText("Assets/Scripts/Utils/Managers/Pool/PoolList.cs", code);
			AssetDatabase.Refresh();
		}
	}
#endif
}