﻿using UnityEngine;

namespace Utils.Managers.Pool
{
	public class PoolObject : MonoBehaviour
	{
		[HideInInspector] public int instanceAmount = 10;

		public PoolObject Clone()
		{
			PoolObject ret = Instantiate(gameObject).GetComponent<PoolObject>();
			return ret;
		}

		public virtual void Show()
		{
		}

		public virtual void Hide()
		{
		}
	}
}