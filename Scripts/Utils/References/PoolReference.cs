using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Utils.Managers.Pool;

namespace Utils.References
{
	[Serializable]
	public class PoolReference
	{
		public string poolObjectName => _poolObjectName;
		[SerializeField] private string _poolObjectName = string.Empty;
	}

#if UNITY_EDITOR
	[CustomPropertyDrawer(typeof(PoolReference))]
	public class PoolReferenceDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			var poolObjectName = property.FindPropertyRelative("_poolObjectName");

			List<string> poolObjects = GetPoolObjects();

			EditorGUI.BeginProperty(position, label, property);
			var state = EditorGUI.Popup(position, label, 0, poolObjects.Select(s => new GUIContent(s)).ToArray());
			EditorGUI.EndProperty();

			property.serializedObject.ApplyModifiedProperties();
		}

		private List<string> GetPoolObjects()
		{
			var poolObjects = new List<string>();

			string[] guids = AssetDatabase.FindAssets("t:Prefab");

			foreach (string guid in guids)
			{
				string path = AssetDatabase.GUIDToAssetPath(guid);
				GameObject go = AssetDatabase.LoadAssetAtPath<GameObject>(path);
				if (go.GetComponent<PoolObject>() != null)
				{
					poolObjects.Add(go.name);
				}
			}
			return poolObjects;
		}
	}
#endif
}