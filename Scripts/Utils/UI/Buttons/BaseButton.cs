﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Utils.UI.Buttons
{
	public class BaseButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
	{
		public Action OnButtonEnter;
		public Action OnButtonExit;
		public Action OnButtonDowned;
		public Action OnButtonUped;
		public Action OnButtonClicked;
		public bool buttonEnabled { get; set; } = true;
		
		private bool _isDowned = false;
		private float _elapsedTime = 0f;
		private Vector2 _clickedPosition;
		
		public void OnPointerDown(PointerEventData pointerEventData)
		{
			if (!buttonEnabled) return;
			_isDowned = true;
			_clickedPosition = Input.mousePosition;
			_elapsedTime = 0f;
			_OnButtonDowned();
			OnButtonDowned?.Invoke();
		}

		public void OnPointerUp(PointerEventData pointerEventData)
		{
			if (!_isDowned) return;
			if (!buttonEnabled) return;
			
			_isDowned = false;
			if (Vector2.Distance(Input.mousePosition, _clickedPosition) > 50) return;
			if (_elapsedTime > 0.5f) return;

			_OnButtonUped();
			OnButtonUped?.Invoke();
			_OnButtonClicked();
			OnButtonClicked?.Invoke();
		}

		public void OnPointerEnter(PointerEventData pointerEventData)
		{
			if (!buttonEnabled) return;
			_OnButtonEnter();
			OnButtonEnter?.Invoke();
		}

		public void OnPointerExit(PointerEventData pointerEventData)
		{
			if (!buttonEnabled) return;
			_isDowned = false;
			_OnButtonExit();
			OnButtonExit?.Invoke();
			_OnButtonUped();
			OnButtonUped?.Invoke();
		}

		protected virtual void Update()
		{
			if (_isDowned) _elapsedTime += Time.deltaTime;
		}

		protected virtual void _OnButtonDowned()
		{
		}

		protected virtual void _OnButtonUped()
		{
		}

		protected virtual void _OnButtonClicked()
		{
		}

		protected virtual void _OnButtonEnter()
		{
		}

		protected virtual void _OnButtonExit()
		{
		}
	}
}