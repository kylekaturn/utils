using System;
using UnityEngine;
using DG.Tweening;

namespace Utils.UI.Buttons
{
	public class ScaleButton : BaseButton
	{
		public float scale = 1.1f;
		private Vector3 _initialScale;

		private void Awake()
		{
			_initialScale = transform.localScale;
		}

		protected override void _OnButtonEnter()
		{
			transform.localScale = _initialScale * scale;
		}

		protected override void _OnButtonExit()
		{
			transform.localScale = _initialScale;
		}

		protected override void _OnButtonClicked()
		{
			buttonEnabled = false;
			DOTween.Kill(transform);
			transform.localScale = _initialScale;
			transform.DOScale(_initialScale * scale, 0.1f).SetEase(Ease.InSine).OnComplete(() =>
			{
				buttonEnabled = true;
			});
		}
	}
}