namespace  Utils.UI.Buttons
{
	public class TestButton : TextButton
	{
		private void Awake()
		{
			OnButtonEnter += () => textField.text = "OnButtonEnter";
			OnButtonClicked += () => textField.text = "OnButtonClicked";
			OnButtonDowned += () => textField.text = "OnButtonDowned";
			OnButtonExit += () => textField.text = "OnButtonExit";
			OnButtonUped += () => textField.text = "OnButtonUped";
		}
	}

}

