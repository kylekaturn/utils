﻿using UnityEngine;

namespace Utils.UI
{

	public class CanvasGroupBlinker : MonoBehaviour
	{
		public float time = 0.1f;
		
		private CanvasGroup _canvasGroup;
		private float _elapsedTime = 0f;

		private void Start()
		{
			_canvasGroup = GetComponent<CanvasGroup>();
		}

		private void Update()
		{
			_elapsedTime += Time.deltaTime;
			if (_elapsedTime > time)
			{
				_elapsedTime = 0f;
				if (_canvasGroup.alpha > 0.9f)
				{
					_canvasGroup.alpha = 0;
				}
				else
				{
					_canvasGroup.alpha = 1;
				}
			}
		}
	}
}
