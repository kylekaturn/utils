using UnityEngine;
using UnityEngine.UI;
using Utils.Extensions;

namespace Utils.UI
{
    public class Progress : MonoBehaviour
    {
        public Image outline;
        public Image bg;
        public Image fill;

        public float progress
        {
            get => fill.transform.localScale.x;
            set => fill.transform.SetScaleX(value);
        }
    }
}
