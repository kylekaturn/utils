﻿using UnityEngine;

namespace Utils.UI
{
	public class SpriteBlinker : MonoBehaviour
	{
		private SpriteRenderer _spriteRenderer;
		private bool _flag = false;

		private void Awake()
		{
			_spriteRenderer = GetComponent<SpriteRenderer>();
		}

		private void Update()
		{
			if (_flag)
			{
				_flag = false;
				return;
			}
			if (_spriteRenderer.color.a > 0.5f)
			{
				_spriteRenderer.color = new Color(0, 0, 0, 0);
			}
			else
			{
				_spriteRenderer.color = new Color(1, 1, 1, 1);
			}
			_flag = true;
		}
	}

}