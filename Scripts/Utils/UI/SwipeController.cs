﻿using System;
using DG.Tweening;
using UnityEngine;
using Utils.Extensions;

namespace Utils.UI
{
	public class SwipeController : MonoBehaviour
	{
		public RectTransform buttonArea;
		public RectTransform contentHolder;
		public bool pageSnap = false;

		private bool _isDragging = false;
		private Vector2 _downedMousePosition;
		private Vector2 _lastMousePosition;
		private Vector2 _downedContentPosition;
		private Vector2 _target;
		private Vector2 _startPosition;

		private Vector2 _velocity;
		[NonSerialized] public readonly float maximumY = 2000;

		private int _screenWidth = 0;
		private int _screenHeight = 0;
		private Tweener _tweener;

		private void Awake()
		{
			_screenWidth = Screen.width;
			_screenHeight = Screen.height;
		}

		private void Start()
		{
			_target = contentHolder.localPosition;
			_startPosition = contentHolder.localPosition;
		}

		public void Reset()
		{
			contentHolder.DOAnchorPosY(_startPosition.y, 0f);
			_target = _startPosition;
		}

		private void Update()
		{
			if (!swipeEnabled) return;

			if (Input.GetMouseButtonDown(0) && !_isDragging)
			{
				bool inside = RectTransformUtility.RectangleContainsScreenPoint(buttonArea, Input.mousePosition, Camera.main);

				if (inside)
				{
					_isDragging = true;
					_downedMousePosition = currentMousePosition;
					_downedContentPosition = contentHolder.localPosition;
				}
			}

			if (Input.GetMouseButtonUp(0) && _isDragging)
			{
				_isDragging = false;
			}

			if (_isDragging)
			{
				Vector2 differ = currentMousePosition - _downedMousePosition;
				_target = _downedContentPosition + differ * 2000;
				if (_target.y > 0) _target.y *= 1.0f;
				contentHolder.SetLocalY(_target.y);
				_velocity = Vector2.Lerp(_velocity, (currentMousePosition - _lastMousePosition) * 2000, 0.5f);
			}
			else
			{
				if (Mathf.Abs(_velocity.y) < 0.01f) _velocity.y = 0f;
				contentHolder.SetLocalY(contentHolder.localPosition.y + _velocity.y);
				_velocity *= 0.9f;
			}

			if (y < 0)
			{
				contentHolder.SetLocalY(Mathf.Lerp(contentHolder.localPosition.y, 0, 0.5f));
				_velocity.y = 0f;
			}
			if (y > maximumY)
			{
				contentHolder.SetLocalY(Mathf.Lerp(contentHolder.localPosition.y, maximumY, 0.5f));
				_velocity.y = 0f;
			}

			_lastMousePosition = currentMousePosition;
		}

		public Vector2 currentMousePosition
		{
			get => new Vector2(Input.mousePosition.x / Screen.width, Input.mousePosition.y / Screen.height);
		}

		public float y
		{
			get => contentHolder.localPosition.y;
			set => contentHolder.SetLocalY(value);
		}

		public bool swipeEnabled { get; set; } = true;
	}
}