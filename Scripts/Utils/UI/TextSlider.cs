using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Utils.UI
{
	public class TextSlider : MonoBehaviour
	{
		public TextMeshProUGUI text;
		public Slider slider;
	}
}
