﻿using TMPro;
using UnityEngine;

namespace Utils.UI
{
    public class TextSync : MonoBehaviour
    {
        public TextMeshProUGUI text;
        public TextMeshProUGUI targetText;

        private void Update()
        {
            text.text = targetText.text;
        }
        
    }
}
