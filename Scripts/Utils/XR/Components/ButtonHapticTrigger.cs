using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.XR.Interaction.Toolkit.UI;

namespace Utils.XR.Components
{
	public class ButtonHapticTrigger : MonoBehaviour, IPointerEnterHandler
	{
		public void OnPointerEnter(PointerEventData pointerEventData)
		{
			if (XRPlayer.Instance == null) return;

			var trackedDeviceEventData = pointerEventData as TrackedDeviceEventData;
			if (trackedDeviceEventData == null) return;
			if (trackedDeviceEventData.interactor.ToString().Contains("Left"))
			{
				if (XRPlayer.Instance != null) XRPlayer.Instance.controllerLeft.TriggerHaptic(0.5f, 0.01f);
			}
			else
			{
				if (XRPlayer.Instance != null) XRPlayer.Instance.controllerRight.TriggerHaptic(0.5f, 0.01f);
			}
		}
	}
}