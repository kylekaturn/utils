using System;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.XR;

namespace Utils.XR.Components
{
	/// <summary>
	/// 반사 컴포넌트
	/// 리플렉션 카메라를 추가하여 렌더텍스쳐에 렌더링하는 플래너 리플랙션
	/// 이 컴퍼넌트와 같은 게임오브젝트에 MeshRenderer 가 존재하고, 해당 MeshRenderer 에 XRReflection 머테리얼이 적용되어있어야한다
	/// </summary>
	public class Reflection : MonoBehaviour
	{
		//Shader id
		private static readonly int _isReflectionCameraID = Shader.PropertyToID("_IsReflectionCamera");

		/// <summary>
		/// 컬링 마스크
		/// </summary>
		public LayerMask cullingMask = -1;

		/// <summary>
		/// 렌더 텍스쳐 사이즈, 최적화를 위해 2의제곱 사이즈로 권장
		/// </summary>
		public int textureSize = 512;

		[Range(0, 1)]
		public float farClipRatio = 1.0f;

		/// <summary>
		/// 밉맵 생성할지 여부
		/// </summary>
		public bool generateMipmap = false;

		//Private Fields
		private Camera _reflectionCamera;
		private UniversalAdditionalCameraData _reflectionCameraData;
		private RenderTexture _reflectionTextureLeft, _reflectionTextureRight;
		private Matrix4x4 reflectionMatrix;
		private Vector3 _leftEyePosition;
		private Vector3 _rightEyePosition;
		private float _ipd;
		private bool _isRendering = false;

		/// <summary>
		/// 리플랙션 카메라 생성 및 카메라 이벤트 설정
		/// </summary>
		private void OnEnable()
		{
			CreateReflectionCamera();
			RenderPipelineManager.beginCameraRendering += UpdateCamera;
		}

		/// <summary>
		/// 리플렉션 카메라 제거 및 카메라 이벤트 해제
		/// </summary>
		private void OnDisable()
		{
			RenderPipelineManager.beginCameraRendering -= UpdateCamera;
			if (_reflectionCamera != null) DestroyImmediate(_reflectionCamera.gameObject);
			if (_reflectionTextureLeft != null) DestroyImmediate(_reflectionTextureLeft);
			if (_reflectionTextureRight != null) DestroyImmediate(_reflectionTextureRight);
		}

		/// <summary>
		/// 렌더텍스쳐 사이즈 변경
		/// </summary>
		public void ChangeTextureSize()
		{
			if (!gameObject.activeInHierarchy) return;
			OnDisable();
			OnEnable();
		}

		/// <summary>
		/// 리플렉션 카메라 생성
		/// </summary>
		private void CreateReflectionCamera()
		{
			GameObject go = new GameObject("Reflection Camera");
			go.transform.SetParent(transform);

			//카메라 생성 및 초기화
			_reflectionCamera = go.AddComponent<Camera>();
			_reflectionCamera.useOcclusionCulling = true;
			_reflectionCamera.enabled = false;
			_reflectionCamera.transform.position = transform.position;
			_reflectionCamera.transform.rotation = transform.rotation;
			_reflectionCamera.cullingMask = cullingMask;
			_reflectionCamera.clearFlags = CameraClearFlags.Skybox;
			_reflectionCamera.cameraType = CameraType.Reflection;
			_reflectionCamera.renderingPath = RenderingPath.VertexLit;
			_reflectionCamera.depthTextureMode = DepthTextureMode.None;
			_reflectionCamera.allowHDR = false;
			_reflectionCamera.allowMSAA = false;

			//카메라 데이터 생성 및 초기화
			_reflectionCameraData = go.AddComponent<UniversalAdditionalCameraData>();
			_reflectionCameraData.antialiasing = AntialiasingMode.None;
			_reflectionCameraData.renderShadows = false;
			_reflectionCameraData.requiresDepthOption = CameraOverrideOption.Off;
			_reflectionCameraData.requiresColorOption = CameraOverrideOption.Off;
			_reflectionCameraData.renderPostProcessing = false;
			_reflectionCameraData.allowXRRendering = true;
			_reflectionCameraData.requiresColorTexture = false;
			_reflectionCameraData.requiresDepthTexture = false;

			//렌더텍스쳐 생성
			_reflectionTextureLeft = new RenderTexture(textureSize, textureSize, 16) {isPowerOfTwo = true, useMipMap = generateMipmap};
			_reflectionTextureRight = new RenderTexture(textureSize, textureSize, 16) {isPowerOfTwo = true, useMipMap = generateMipmap};

			//리플렉션 매트릭스 생성
			Vector3 mirrorPos = transform.position + new Vector3(0, 0.1f, 0);
			Vector3 mirrorNormal = transform.up * -1f;
			float clippingPlaneOffset = 0.09f;
			float d = (-Vector3.Dot(mirrorNormal, mirrorPos) - clippingPlaneOffset);
			Vector4 reflectionPlane = new Vector4(mirrorNormal.x, mirrorNormal.y, mirrorNormal.z, d);
			CalculateReflectMatrix(ref reflectionMatrix, reflectionPlane);

			//머테리얼에 텍스쳐 적용
			Material.SetTexture("_ReflectionTextureLeft", _reflectionTextureLeft);
			Material.SetTexture("_ReflectionTextureRight", _reflectionTextureRight);
			Material.SetFloat("_IsStereo", 1);
		}

		/// <summary>
		/// 카메라 업데이트
		/// </summary>
		/// <param name="context">렌더 컨텍스트</param>
		/// <param name="camera">메인 카메라</param>
		private void UpdateCamera(ScriptableRenderContext context, Camera camera)
		{
			//메인카메라일 경우에만 리플렉션 렌더링
			if (!camera.CompareTag("MainCamera")) return;

			if (_isRendering) return;
			_isRendering = true;

			//기본적인 카메라 속성들 카피
			_reflectionCamera.CopyFrom(camera);
			_reflectionCamera.backgroundColor = camera.backgroundColor;
			_reflectionCamera.cullingMask = cullingMask;
			_reflectionCamera.clearFlags = camera.clearFlags;

			//ipd 계산
			InputDevice centerEye = InputDevices.GetDeviceAtXRNode(XRNode.CenterEye);
			centerEye.TryGetFeatureValue(CommonUsages.leftEyePosition, out _leftEyePosition);
			centerEye.TryGetFeatureValue(CommonUsages.rightEyePosition, out _rightEyePosition);
			_ipd = Vector3.Distance(_leftEyePosition, _rightEyePosition) * camera.transform.lossyScale.x;

			//쉐이더에서 카메라 구별을 위한 변수 적용뒤 양안 리플렉션카메라 렌더링
			Shader.SetGlobalFloat(_isReflectionCameraID, 1);
			RenderReflectionCamera(context, camera, Camera.StereoscopicEye.Left);
			RenderReflectionCamera(context, camera, Camera.StereoscopicEye.Right);
			Shader.SetGlobalFloat(_isReflectionCameraID, 0);

			_isRendering = false;
		}

		/// <summary>
		/// 리플렉션 카메라 렌더링
		/// </summary>
		/// <param name="context">렌더 컨텍스트</param>
		/// <param name="camera">메인카메라</param>
		/// <param name="eye"></param>
		private void RenderReflectionCamera(ScriptableRenderContext context, Camera camera, Camera.StereoscopicEye eye)
		{
			Vector3 originalPos = camera.transform.position;
			camera.transform.position += eye == Camera.StereoscopicEye.Left ? -(camera.transform.right * _ipd / 2f) : (camera.transform.right * _ipd / 2f);

			//타겟 렌더텍스쳐 적용
			_reflectionCamera.targetTexture = eye == Camera.StereoscopicEye.Left ? _reflectionTextureLeft : _reflectionTextureRight;

			//프로젝션 매트릭스 FarClipPlane 변경하고 스테레오 매트릭스로 적용
			Matrix4x4 projectionMatrix = Application.isEditor ? camera.projectionMatrix : camera.GetStereoProjectionMatrix(eye);
			float farClipPlane = camera.farClipPlane * farClipRatio;
			float C = (farClipPlane + camera.nearClipPlane) / (camera.nearClipPlane - farClipPlane);
			float D = (2 * farClipPlane * camera.nearClipPlane) / (camera.nearClipPlane - farClipPlane);
			projectionMatrix[2, 2] = C;
			projectionMatrix[2, 3] = D;
			_reflectionCamera.projectionMatrix = projectionMatrix;

			//월드투카메라 매트릭스 적용
			_reflectionCamera.worldToCameraMatrix = camera.worldToCameraMatrix * reflectionMatrix;

			//컬링 반전시킴
			GL.invertCulling = true;
#pragma warning disable CS0618
			//카메라 렌더링
			UniversalRenderPipeline.RenderSingleCamera(context, _reflectionCamera);
#pragma warning restore CS0618
			GL.invertCulling = false;

			camera.transform.position = originalPos;
		}

		/// <summary>
		/// 리플렉션 매트릭스 계산
		/// </summary>
		/// <param name="reflectionMatrix">매트릭스</param>
		/// <param name="reflectionPlane">반사 기준 플레인</param>
		private static void CalculateReflectMatrix(ref Matrix4x4 reflectionMatrix, Vector4 reflectionPlane)
		{
			reflectionMatrix.m00 = (1F - 2F * reflectionPlane[0] * reflectionPlane[0]);
			reflectionMatrix.m01 = (-2F * reflectionPlane[0] * reflectionPlane[1]);
			reflectionMatrix.m02 = (-2F * reflectionPlane[0] * reflectionPlane[2]);
			reflectionMatrix.m03 = (-2F * reflectionPlane[3] * reflectionPlane[0]);

			reflectionMatrix.m10 = (-2F * reflectionPlane[1] * reflectionPlane[0]);
			reflectionMatrix.m11 = (1F - 2F * reflectionPlane[1] * reflectionPlane[1]);
			reflectionMatrix.m12 = (-2F * reflectionPlane[1] * reflectionPlane[2]);
			reflectionMatrix.m13 = (-2F * reflectionPlane[3] * reflectionPlane[1]);

			reflectionMatrix.m20 = (-2F * reflectionPlane[2] * reflectionPlane[0]);
			reflectionMatrix.m21 = (-2F * reflectionPlane[2] * reflectionPlane[1]);
			reflectionMatrix.m22 = (1F - 2F * reflectionPlane[2] * reflectionPlane[2]);
			reflectionMatrix.m23 = (-2F * reflectionPlane[3] * reflectionPlane[2]);

			reflectionMatrix.m30 = 0F;
			reflectionMatrix.m31 = 0F;
			reflectionMatrix.m32 = 0F;
			reflectionMatrix.m33 = 1F;
		}


		private Material _material;
		/// <summary>
		/// 머테리얼 캐싱 프로퍼티
		/// </summary>
		private Material Material
		{
			get
			{
				if (!_material) _material = GetComponent<MeshRenderer>().material;
				return _material;
			}
		}
	}
}