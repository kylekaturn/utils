using UnityEngine;
using Utils.Extensions;

namespace Utils.XR.Consoles
{
	public class ConsoleHolder : MonoBehaviour
	{
		public enum Level { None, MonitorOnly, RenderSettingOnly, LogOnly, Temp }

		public MonitorConsole monitorConsole;
		public RenderSettingConsole renderSettingConsole;
		public LogConsole logConsole;
		public GPUTimeConsole gpuTimeConsole;

		private Level _level = Level.Temp;

		public Level level
		{
			get => _level;
			set
			{
				if (_level == value) return;
				_level = value;
				switch (value)
				{
					case Level.None:
						monitorConsole.gameObject.SetActive(false);
						renderSettingConsole.Hide();
						logConsole.Hide();
						break;
					case Level.MonitorOnly:
						monitorConsole.gameObject.SetActive(true);
						renderSettingConsole.Hide();
						logConsole.Hide();
						break;
					case Level.RenderSettingOnly:
						monitorConsole.gameObject.SetActive(false);
						renderSettingConsole.Show();
						logConsole.Hide();
						break;
					case Level.LogOnly:
						monitorConsole.gameObject.SetActive(false);
						renderSettingConsole.Hide();
						StartCoroutine(new WaitForEndOfFrame().Then(logConsole.Show));
						break;
				}
			}
		}

		public Level nextLevel => level switch
		{
			Level.None => Level.MonitorOnly,
			Level.MonitorOnly => Level.RenderSettingOnly,
			Level.RenderSettingOnly => Level.LogOnly,
			Level.LogOnly => Level.None,
			_ => Level.None
		};
	}
}