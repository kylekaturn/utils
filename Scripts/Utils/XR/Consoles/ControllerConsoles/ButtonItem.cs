using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Utils.XR.Consoles.ControllerConsoles
{
	public class ButtonItem : MonoBehaviour
	{
		public TextMeshProUGUI label;
		public Button button;

		public void Setup(string title, UnityAction OnClicked)
		{
			label.text = title;
			button.onClick.AddListener(OnClicked);
		}
	}
}