using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Utils.XR.Consoles.ControllerConsoles
{
	public class EnumItem : MonoBehaviour
	{
		public TextMeshProUGUI label;
		public ToggleGroup toggleGroup;
		public ToggleItem toggleItemPrefab;
		//private List<ToggleItem> _toggleItems;

		public static T Parse<T>(String value) where T : struct
		{
			return (T) Enum.Parse(typeof(T), value);
		}

		public void Setup<T>(string title, T defaultValue, Action<T> OnValueChanged) where T : struct
		{
			label.text = title;
			foreach (var enumValue in Enum.GetValues(typeof(T)))
			{
				ToggleItem toggleItem = Instantiate(toggleItemPrefab, GetComponent<RectTransform>());
				toggleItem.Setup(enumValue.ToString(), false, value =>
				{
					if (value) OnValueChanged?.Invoke(Parse<T>(enumValue.ToString()));
				});
				toggleItem.toggle.group = toggleGroup;
				if (enumValue.ToString() == defaultValue.ToString()) toggleItem.toggle.isOn = true;
			}
			toggleGroup.enabled = true;
		}
	}
}