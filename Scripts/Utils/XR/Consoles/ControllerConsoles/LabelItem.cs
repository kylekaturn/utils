using TMPro;
using UnityEngine;

namespace Utils.XR.Consoles.ControllerConsoles
{
	public class LabelItem : MonoBehaviour
	{
		public TextMeshProUGUI label;

		public void Setup(string title)
		{
			label.SetText(title);
		}

		public void SetText(string text)
		{
			label.SetText(text);
		}
	}
}