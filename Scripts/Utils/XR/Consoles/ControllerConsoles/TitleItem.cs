using TMPro;
using UnityEngine;

namespace Utils.XR.Consoles.ControllerConsoles
{
	public class TitleItem : MonoBehaviour
	{
		public TextMeshProUGUI label;

		public void Setup(string title)
		{
			label.text = title;
		}
	}
}