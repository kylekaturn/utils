using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Utils.XR.Consoles.ControllerConsoles
{
	public class ToggleGroupItem : MonoBehaviour
	{
		public TextMeshProUGUI label;
		public ToggleGroup toggleGroup;
		public ToggleItem toggleItemPrefab;
		private List<ToggleItem> _toggleItems;

		public void Setup(string title, List<string> titles, Action<int> OnValueChanged)
		{
			label.text = title;
			_toggleItems = new List<ToggleItem>();

			for (int index = 0; index < titles.Count; index++)
			{
				ToggleItem toggleItem = Instantiate(toggleItemPrefab, GetComponent<RectTransform>());
				_toggleItems.Add(toggleItem);
				toggleItem.Setup(titles[index], false, value =>
				{
					if (value) OnValueChanged?.Invoke(_toggleItems.IndexOf(toggleItem));
				});
				toggleItem.toggle.group = toggleGroup;
				if (index == 0) toggleItem.toggle.isOn = true;
			}

			toggleGroup.enabled = true;
		}
	}
}