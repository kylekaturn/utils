using System;
using TMPro;
using Unity.XR.Oculus;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.UI;
using UnityEngine.XR;
using Utils.Attributes;
using Utils.Extensions;

namespace Utils.XR.Consoles
{
	public class MonitorConsole : MonoBehaviour
	{
		[Serializable]
		public class ColorLevel
		{
			public enum Type { Ascend, Descend }
			public Color goodColor = Color.green;
			public Color warnColor = Color.yellow;
			public Color badColor = Color.red;
			public float goodThreshold = 72;
			public float warnThreshold = 60;
			public Type type = Type.Ascend;
		}

		[Separator("Configuration")]
		[Range(0.1f, 1f)] public float pollingRate = 1f;

		[Separator("Stats Color Level")]
		public ColorLevel gpuTimeColorLevel;
		public ColorLevel fpsColorLevel;
		public ColorLevel renderTimeColorLevel;
		public ColorLevel gpuUtilColorlLevel;
		public ColorLevel cpuUtilAverageColorlLevel;

		[Separator("Items")]
		public TextMeshProUGUI statsText;

		private float _elapsedTime = 0f;
		private int _frameCount = 0;
		private float _averageFPS = 0;
		private float _averageMS = 0;

		private void Start()
		{
// 			buildText.text = @$"
// Identifier : {Application.identifier}
// Build Version : {Application.version}
// Unity Version : {Application.unityVersion}
// Graphics API : {SystemInfo.graphicsDeviceType.ToString()}
// Platform : {Application.platform.ToString()}
// Spatializer : {AudioSettings.GetSpatializerPluginName()}
// ";
		}

		private void Update()
		{
			XRPlayer xrPlayer = XRPlayer.Instance;
			_elapsedTime += Time.unscaledDeltaTime;
			_frameCount++;

			if (_elapsedTime < pollingRate) return;
			_averageFPS = _frameCount / _elapsedTime;
			_averageMS = _elapsedTime / _frameCount;

			if (XRPlayer.platform != XRPlatform.Device) return;

			statsText.text = @$"
App GPU Time : {FormatColor(Stats.PerfMetrics.AppGPUTime * 1000000, gpuTimeColorLevel, "F0")}
GPU Util : {FormatColor(Stats.PerfMetrics.GPUUtilization, gpuUtilColorlLevel, "F0", " %")}
GPU Clock :  {Stats.PerfMetrics.GPUClockFrequency:F5}
GPU Level : {Stats.AdaptivePerformance.GPULevel}

App Cpu Time :  {Stats.PerfMetrics.AppCPUTime * 1000000}
CPU Util : {FormatColor(Stats.PerfMetrics.CPUUtilizationAverage, cpuUtilAverageColorlLevel, "F0", " %")}
CPU Clock :  {Stats.PerfMetrics.CPUClockFrequency:F5}
CPU Level : {Stats.AdaptivePerformance.CPULevel}

FPS : {FormatColor(_averageFPS, fpsColorLevel, "F0", " fps")}
Render Time : {FormatColor(_averageMS * 1000, renderTimeColorLevel, "F2", " ms")}

Memory Reserved : {(Profiler.GetTotalReservedMemoryLong() / 1048576f).ToString("F0") + " mb"}
Memory Allocated : {(Profiler.GetTotalAllocatedMemoryLong() / 1048576f).ToString("F0") + " mb"}
Memory Mono : {(Profiler.GetMonoHeapSizeLong() / 1048576f).ToString("F0") + " mb"}

FFR Level : {Unity.XR.Oculus.Utils.foveatedRenderingLevel}
Use DynamicFoveatedRendering : {Unity.XR.Oculus.Utils.useDynamicFoveatedRendering}
";
			_elapsedTime = 0f;
			_frameCount = 0;
		}

		private string FormatColor(float value, ColorLevel colorLevel, string numFormat, string suffix = "")
		{
			Color color = (value, colorLevel) switch
			{
				var (x, y) when (x >= y.goodThreshold && y.type == ColorLevel.Type.Ascend) => colorLevel.goodColor,
				var (x, y) when (x <= y.goodThreshold && y.type == ColorLevel.Type.Descend) => colorLevel.goodColor,
				var (x, y) when (x >= y.warnThreshold && y.type == ColorLevel.Type.Ascend) => colorLevel.warnColor,
				var (x, y) when (x <= y.warnThreshold && y.type == ColorLevel.Type.Descend) => colorLevel.warnColor,
				_ => colorLevel.badColor
			};
			return $"<color={color.ToHexString()}>{value.ToString(numFormat)}</color> {suffix}";
		}
	}
}