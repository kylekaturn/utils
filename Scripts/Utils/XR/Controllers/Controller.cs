using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit;
using Utils.Extensions;

namespace Utils.XR.Controllers
{
	public class Controller : MonoBehaviour
	{
		public ControllerType controllerType = ControllerType.Left;
		public ActionBasedController controller;
		public UIInteractor uiInteractor;

		private PlayerInputActions playerInputActions => XRPlayer.Instance.playerInputActions;
		public InputAction JoyStick => controllerType == ControllerType.Left ? playerInputActions.LeftHand.Joystick : playerInputActions.RightHand.Joystick;
		public InputAction Trigger => controllerType == ControllerType.Left ? playerInputActions.LeftHand.Trigger : playerInputActions.RightHand.Trigger;
		public InputAction Grip => controllerType == ControllerType.Left ? playerInputActions.LeftHand.Grip : playerInputActions.RightHand.Grip;
		public InputAction TriggerButton => controllerType == ControllerType.Left ? playerInputActions.LeftHand.TriggerButton : playerInputActions.RightHand.TriggerButton;
		public InputAction GripButton => controllerType == ControllerType.Left ? playerInputActions.LeftHand.GripButton : playerInputActions.RightHand.GripButton;
		public InputAction PrimaryButton => controllerType == ControllerType.Left ? playerInputActions.LeftHand.PrimaryButton : playerInputActions.RightHand.PrimaryButton;
		public InputAction SecondaryButton => controllerType == ControllerType.Left ? playerInputActions.LeftHand.SecondaryButton : playerInputActions.RightHand.SecondaryButton;
		public InputAction JoystickButton => controllerType == ControllerType.Left ? playerInputActions.LeftHand.JoystickButton : playerInputActions.RightHand.JoystickButton;
		public InputAction JoystickButtonShort => controllerType == ControllerType.Left ? playerInputActions.LeftHand.JoystickButtonShort : playerInputActions.RightHand.JoystickButtonShort;
		public InputAction JoystickButtonLong => controllerType == ControllerType.Left ? playerInputActions.LeftHand.JoystickButtonLong : playerInputActions.RightHand.JoystickButtonLong;
		public InputAction TrackingState => controllerType == ControllerType.Left ? playerInputActions.LeftHand.TrackingState : playerInputActions.RightHand.TrackingState;
		public InputAction UIPress => controllerType == ControllerType.Left ? playerInputActions.LeftHand.UIPress : playerInputActions.RightHand.UIPress;
		public InputAction Turn => playerInputActions.RightHand.Turn;

		public Vector2 GetJoystickValue() => JoyStick.ReadValue<Vector2>();
		public float GetTriggerValue() => Trigger.ReadValue<float>();
		public float GetGripValue() => Grip.ReadValue<float>();
		public float GetPrimaryButton() => PrimaryButton.ReadValue<float>();
		public float GetSecondaryButton() => SecondaryButton.ReadValue<float>();
		public float GetJoystickButton() => JoystickButton.ReadValue<float>();
		public float GetUIPress() => UIPress.ReadValue<float>();
		public InputTrackingState GetTrackingState() => (InputTrackingState) TrackingState.ReadValue<int>();

		private void LateUpdate()
		{
			uiInteractor.uiSelect = GetUIPress() > 0.5f;
		}

		public void TriggerHaptic(float amplitude = 0.5f, float duration = 0.01f)
		{
			controller.SendHapticImpulse(amplitude, duration);
		}

		public void SetParentOfUIInteractor(Transform parent)
		{
			uiInteractor.transform.SetParent(parent);
			uiInteractor.transform.Reset(true);
		}

		public bool uiInteractorEnabled
		{
			get => uiInteractor.gameObject.activeSelf;
			set => uiInteractor.gameObject.SetActive(value);
		}

		public float uiInteractorDrawDistnace
		{
			get => uiInteractor.drawDistance;
			set => uiInteractor.drawDistance = value;
		}

		public bool isHittingUI
		{
			get => uiInteractor.isHittingUI;
		}
	}
}