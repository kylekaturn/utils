using System;
using UnityEngine;

namespace Utils.XR.Controllers
{
	public class ControllerFollower : MonoBehaviour
	{
		public ControllerType controllerType = ControllerType.Right;
		private Vector3 _initialPosition;
		private Quaternion _initialRotation;

		private void Awake()
		{
			_initialPosition = transform.localPosition;
			_initialRotation = transform.localRotation;
			transform.SetParent(controllerType == ControllerType.Left ? XRPlayer.Instance.controllerLeft.transform : XRPlayer.Instance.controllerRight.transform);
			transform.localPosition = _initialPosition;
			transform.localRotation = _initialRotation;
		}

		private void LateUpdate()
		{
			if (XRPlayer.Instance == null) return;
			if (controllerType == ControllerType.Left)
			{
				//transform.position = XRPlayer.Instance.controllerLeft.transform.position + initialPosition;
				//transform.rotation = XRPlayer.Instance.controllerLeft.transform.rotation * initialRotation;
			}
			else
			{
				//transform.position = XRPlayer.Instance.controllerRight.transform.position + initialPosition;
				//transform.rotation = XRPlayer.Instance.controllerRight.transform.rotation * initialRotation;
			}
		}
	}
}