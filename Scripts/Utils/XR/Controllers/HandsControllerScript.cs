using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

namespace Utils.XR.Controllers
{
	public class HandsControllerScript : MonoBehaviour
	{
		[SerializeField] private InputActionReference _gripInputAction;
		[SerializeField] private InputActionReference _triggerInputAction;
		[SerializeField] private Animator _handAnimator;

		private void OnEnable()
		{
			_gripInputAction.action.performed += OnGripPressed;
			_triggerInputAction.action.performed += OnTriggerPressed;
		}

		private void OnTriggerPressed(InputAction.CallbackContext obj)
		{
			_handAnimator.SetFloat("Trigger", obj.ReadValue<float>());
		}

		private void OnGripPressed(InputAction.CallbackContext obj)
		{
			_handAnimator.SetFloat("Grip", obj.ReadValue<float>());
		}

		private void OnDisable()
		{
			_gripInputAction.action.performed -= OnGripPressed;
			_triggerInputAction.action.performed -= OnTriggerPressed;
		}
	}
}