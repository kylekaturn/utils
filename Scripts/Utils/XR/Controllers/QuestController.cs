using UnityEngine;

namespace Utils.XR.Controllers
{
	public class QuestController : MonoBehaviour
	{
		public ControllerType controllerType = ControllerType.Left;
		public Animator animator;

		protected virtual void Update()
		{
			animator.SetFloat("Joy X", XRPlayer.Instance.GetController(controllerType).GetJoystickValue().x * (int) controllerType);
			animator.SetFloat("Joy Y", XRPlayer.Instance.GetController(controllerType).GetJoystickValue().y);
			animator.SetFloat("Grip", XRPlayer.Instance.GetController(controllerType).GetGripValue());
			animator.SetFloat("Trigger", XRPlayer.Instance.GetController(controllerType).GetTriggerValue());
			animator.SetFloat("Button 1", XRPlayer.Instance.GetController(controllerType).GetPrimaryButton());
			animator.SetFloat("Button 2", XRPlayer.Instance.GetController(controllerType).GetSecondaryButton());
		}

		public bool visible
		{
			get => gameObject.activeSelf;
			set => gameObject.SetActive(value);
		}
	}
}