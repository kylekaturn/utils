using UnityEditor.Rendering;
using UnityEngine;
using Utils.Extensions;

namespace Utils.XR.Controllers
{
	public class UIInteractorGuide : MonoBehaviour
	{
		private UIInteractor _uiInteractor;
		public bool uiInteractorEnabled = true;
		public float drawDistance = 1f;

		private void OnDrawGizmosSelected()
		{
			DebugExtensions.DrawArrow(transform.position, transform.position + transform.forward * 0.5f, Color.red);
		}

		public void SetInteractor(UIInteractor uiInteractor)
		{
			this._uiInteractor = uiInteractor;
			this._uiInteractor.transform.SetParent(transform);
			uiInteractor.drawDistance = drawDistance;
			uiInteractor.transform.Reset(true);
			uiInteractor.gameObject.SetActive(uiInteractorEnabled);
			if (!uiInteractorEnabled) uiInteractor.transform.position = new Vector3(9999, 9999, 9999);
		}
	}
}