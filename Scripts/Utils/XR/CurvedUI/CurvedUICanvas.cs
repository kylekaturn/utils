using System;
using Unity.XR.CoreUtils;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit.UI;
using Utils.Extensions;

namespace Utils.XR.CurvedUI
{
	[RequireComponent(typeof(Canvas), typeof(XRCanvas))]
	[DisallowMultipleComponent]
	public class CurvedUICanvas : MonoBehaviour
	{
		public enum Direction { Horizontal, Vertical }
		public enum BendingStrength { Angle5 = 5, Angle10 = 10, Angle15 = 15, Angle20 = 20 }

		public Direction direction = Direction.Horizontal;
		public BendingStrength bendingStrength = BendingStrength.Angle5;
		[Range(0.1f, 2f)] public float renderScale = 1.0f;
		public bool hideCanvas = true;
		public Vector3 projectionOffset = new Vector3(0, 0, 0);

		public Canvas canvas { get; private set; }
		public Camera renderCamera { get; private set; }
		public RenderTexture renderTexture { get; private set; }
		public CurvedUIProjection curvedUIProjection { get; private set; }

		private RectTransform _rectTransform => canvas.transform as RectTransform;
		private int _width => (int) _rectTransform.rect.width;
		private int _height => (int) _rectTransform.rect.height;
		private float _scale => (canvas.transform.localScale.x + canvas.transform.localScale.y) / 2f;

		private void OnEnable()
		{
			if (canvas == null) canvas = GetComponent<Canvas>();
			if (renderCamera == null) renderCamera = CreateRenderCamera();
			if (renderTexture == null) renderTexture = CreateRenderTexture();
			if (curvedUIProjection == null) curvedUIProjection = CreateProjection();

			curvedUIProjection.SetTexture(renderTexture);
			renderCamera.targetTexture = renderTexture;

			if (hideCanvas) gameObject.SetLayerRecursively(LayerMask.NameToLayer("UIHidden"));
			if (GetComponent<GraphicRaycaster>()) GetComponent<GraphicRaycaster>().enabled = false;
			if (GetComponent<TrackedDeviceGraphicRaycaster>()) GetComponent<TrackedDeviceGraphicRaycaster>().enabled = false;
			if (GetComponent<CurvedUIRaycasterTrackedDevice>()) GetComponent<CurvedUIRaycasterTrackedDevice>().enabled = true;
			if (GetComponent<CurvedUIRaycasterMouse>()) GetComponent<CurvedUIRaycasterMouse>().enabled = true;
		}

		private void OnDisable()
		{
			if (renderCamera != null) DestroyImmediate(renderCamera.gameObject);
			if (renderTexture != null) DestroyImmediate(renderTexture);
			if (curvedUIProjection != null) DestroyImmediate(curvedUIProjection.gameObject);
			
			if (hideCanvas) gameObject.SetLayerRecursively(LayerMask.NameToLayer("UI"));
			if (GetComponent<GraphicRaycaster>()) GetComponent<GraphicRaycaster>().enabled = true;
			if (GetComponent<TrackedDeviceGraphicRaycaster>()) GetComponent<TrackedDeviceGraphicRaycaster>().enabled = true;
			if (GetComponent<CurvedUIRaycasterTrackedDevice>()) GetComponent<CurvedUIRaycasterTrackedDevice>().enabled = false;
			if (GetComponent<CurvedUIRaycasterMouse>()) GetComponent<CurvedUIRaycasterMouse>().enabled = false;
		}

		private void Update()
		{
			if (canvas == null) return;
			if (renderCamera == null) return;
			if (renderTexture == null) return;
			if (curvedUIProjection == null) return;

			renderCamera.aspect = (float) _width / _height;
			renderCamera.orthographicSize = _height * 0.5f * _scale;
			curvedUIProjection.transform.localScale = new Vector3(_width * _scale, _height * _scale, _width * _scale);
			curvedUIProjection.transform.position = transform.position + projectionOffset;
			curvedUIProjection.transform.rotation = transform.rotation;
			curvedUIProjection.transform.Rotate(new Vector3(0, 180, 0));
		}

		private Camera CreateRenderCamera()
		{
			GameObject go = new GameObject("CurvedUI Camera");
			go.transform.SetParent(transform);
			go.transform.localScale = Vector3.one;
			go.transform.localPosition = new Vector3(0, 0, -10);
			go.transform.localRotation = Quaternion.identity;

			Camera createdCamera = go.AddComponent<Camera>();
			createdCamera.enabled = true;
			createdCamera.cullingMask = LayerMask.GetMask(hideCanvas ? "UIHidden" : "UI");
			createdCamera.clearFlags = CameraClearFlags.SolidColor;
			createdCamera.orthographic = true;
			createdCamera.backgroundColor = "#00000000".ToColor();
			createdCamera.nearClipPlane = 0f;
			createdCamera.farClipPlane = 0.2f;

			UniversalAdditionalCameraData renderCameraData = go.AddComponent<UniversalAdditionalCameraData>();
			renderCameraData.renderShadows = false;
			renderCameraData.renderPostProcessing = false;
			renderCameraData.requiresColorTexture = false;
			renderCameraData.requiresDepthTexture = false;

			return createdCamera;
		}

		public CurvedUIProjection CreateProjection()
		{
			GameObject go = new GameObject(gameObject.name + "CurvedUIProjection");
			go.layer = LayerMask.NameToLayer("UI");
			//go.hideFlags = HideFlags.HideInHierarchy;
			CurvedUIProjection projection = curvedUIProjection = go.AddComponent<CurvedUIProjection>();
			MeshFilter meshFilter = go.AddComponent<MeshFilter>();
			MeshRenderer meshRenderer = go.AddComponent<MeshRenderer>();
			MeshCollider meshCollider = go.AddComponent<MeshCollider>();
			Material material = new Material(Shader.Find("XR/CurvedUI"));
			Mesh mesh = Resources.Load<Mesh>("Meshes/Bend" + (int) bendingStrength);
			meshRenderer.material = material;
			meshRenderer.shadowCastingMode = ShadowCastingMode.Off;
			meshFilter.mesh = mesh;
			meshCollider.sharedMesh = mesh;
			return projection;
		}

		private RenderTexture CreateRenderTexture()
		{
			return new RenderTexture((_width * renderScale).ToInt(), (_height * renderScale).ToInt(), 16);
		}
	}
}