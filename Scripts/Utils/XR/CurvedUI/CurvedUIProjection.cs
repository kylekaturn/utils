using System;
using UnityEngine;

namespace Utils.XR.CurvedUI
{
    public class CurvedUIProjection : MonoBehaviour
    {
        public void SetTexture(Texture texture)
        {
            GetComponent<MeshRenderer>().material.mainTexture = texture;
        }
    }
}
