using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Utils.Extensions;

namespace Utils.XR.CurvedUI
{
	[RequireComponent(typeof(Canvas), typeof(CurvedUICanvas))]
	[DisallowMultipleComponent]
	public class CurvedUIRaycasterMouse : BaseRaycaster
	{
		private CurvedUICanvas _curvedUICanvas;
		public override Camera eventCamera => _canvas.worldCamera;
		private Canvas _canvas => _curvedUICanvas.canvas;
		private CurvedUIProjection _projection => _curvedUICanvas.curvedUIProjection;
		private Camera _renderCamera => _curvedUICanvas.renderCamera;

		[NonSerialized] private List<Graphic> m_RaycastResults = new List<Graphic>();

		protected override void Awake()
		{
			base.Awake();
			_curvedUICanvas = GetComponent<CurvedUICanvas>();
		}

		public override void Raycast(PointerEventData eventData, List<RaycastResult> resultAppendList)
		{
			if (!_curvedUICanvas.enabled) return;
			var canvasGraphics = GraphicRegistry.GetRaycastableGraphicsForCanvas(_canvas);
			if (canvasGraphics == null || canvasGraphics.Count == 0) return;
			float dist = Vector2.Distance(eventData.position, Input.mousePosition);
			if (dist > 0) return;
			m_RaycastResults.Clear();

			Ray ray = new Ray();
			ray = eventCamera.ScreenPointToRay(eventData.position);
			
			if (Physics.Raycast(ray, out RaycastHit hitInfo, 999))
			{
				if (hitInfo.collider.gameObject != _projection.gameObject) return;
				Vector3 canvasHitPosition = _canvas.GetHitPositionByCoord(hitInfo.textureCoord);
				eventData.position = _renderCamera.WorldToScreenPoint(canvasHitPosition);
			}
			else return;

			ActualRaycast(_canvas, _renderCamera, eventData.position, canvasGraphics, m_RaycastResults);

			int totalCount = m_RaycastResults.Count;
			for (var index = 0; index < totalCount; index++)
			{
				var go = m_RaycastResults[index].gameObject;
				float distance = 0;

				distance = (Vector3.Dot(go.transform.forward, go.transform.position - ray.origin) / Vector3.Dot(go.transform.forward, ray.direction));
				if (distance < 0) continue;

				var castResult = new RaycastResult
				{
					gameObject = go,
					module = this,
					distance = distance,
					screenPosition = eventData.position,
					displayIndex = eventCamera.targetDisplay,
					index = resultAppendList.Count,
					depth = m_RaycastResults[index].depth,
					sortingLayer = _canvas.sortingLayerID,
					sortingOrder = _canvas.sortingOrder,
					worldPosition = ray.origin + ray.direction * distance,
					worldNormal = -go.transform.forward
				};
				resultAppendList.Add(castResult);
			}
		}


		[NonSerialized] static readonly List<Graphic> s_SortedGraphics = new List<Graphic>();

		private static void ActualRaycast(Canvas canvas, Camera eventCamera, Vector2 pointerPosition, IList<Graphic> foundGraphics, List<Graphic> results)
		{
			int totalCount = foundGraphics.Count;
			for (int i = 0; i < totalCount; ++i)
			{
				Graphic graphic = foundGraphics[i];
				if (!graphic.raycastTarget || graphic.canvasRenderer.cull || graphic.depth == -1) continue;
				if (!RectTransformUtility.RectangleContainsScreenPoint(graphic.rectTransform, pointerPosition, eventCamera, graphic.raycastPadding)) continue;
				if (eventCamera != null && eventCamera.WorldToScreenPoint(graphic.rectTransform.position).z > eventCamera.farClipPlane) continue;
				if (graphic.Raycast(pointerPosition, eventCamera))
				{
					s_SortedGraphics.Add(graphic);
				}
			}

			s_SortedGraphics.Sort((g1, g2) => g2.depth.CompareTo(g1.depth));
			totalCount = s_SortedGraphics.Count;
			for (int i = 0; i < totalCount; ++i) results.Add(s_SortedGraphics[i]);
			s_SortedGraphics.Clear();
		}
	}
}