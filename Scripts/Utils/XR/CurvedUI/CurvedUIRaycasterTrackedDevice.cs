using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit.UI;
using Utils.Extensions;

namespace Utils.XR.CurvedUI
{
	[RequireComponent(typeof(Canvas), typeof(CurvedUICanvas))]
	[DisallowMultipleComponent]
	public class CurvedUIRaycasterTrackedDevice : BaseRaycaster
	{
		private CurvedUICanvas _curvedUICanvas;
		private Canvas _canvas => _curvedUICanvas.canvas;
		public override Camera eventCamera => _canvas.worldCamera;
		private CurvedUIProjection _projection => _curvedUICanvas.curvedUIProjection;
		
		private readonly struct RaycastHitData
		{
			public RaycastHitData(Graphic graphic, Vector3 worldHitPosition, Vector2 screenPosition, float distance, int displayIndex)
			{
				this.graphic = graphic;
				this.worldHitPosition = worldHitPosition;
				this.screenPosition = screenPosition;
				this.distance = distance;
				this.displayIndex = displayIndex;
			}

			public Graphic graphic { get; }
			public Vector3 worldHitPosition { get; }
			public Vector2 screenPosition { get; }
			public float distance { get; }
			public int displayIndex { get; }
		}
		
		[SerializeField]
		bool m_IgnoreReversedGraphics;

		public bool ignoreReversedGraphics
		{
			get => m_IgnoreReversedGraphics;
			set => m_IgnoreReversedGraphics = value;
		}

		public override void Raycast(PointerEventData eventData, List<RaycastResult> resultAppendList)
		{
			if (!_curvedUICanvas.enabled) return;
			if (eventData is TrackedDeviceEventData trackedEventData)
			{
				PerformRaycasts(trackedEventData, resultAppendList);
			}
		}

		bool m_HasWarnedEventCameraNull;
		static readonly Vector3[] s_Corners = new Vector3[4];
		
		readonly List<RaycastHitData> m_RaycastResultsCache = new List<RaycastHitData>();

		[NonSerialized]
		static readonly List<RaycastHitData> s_SortedGraphics = new List<RaycastHitData>();

		protected override void Awake()
		{
			base.Awake();
			_curvedUICanvas = GetComponent<CurvedUICanvas>();
		}

		void PerformRaycasts(TrackedDeviceEventData eventData, List<RaycastResult> resultAppendList)
		{
			if (_canvas == null) return;

			var currentEventCamera = eventCamera;
			if (currentEventCamera == null) return;

			var rayPoints = eventData.rayPoints;
			var layerMask = eventData.layerMask;
			for (var i = 1; i < rayPoints.Count; i++)
			{
				var from = rayPoints[i - 1];
				var to = rayPoints[i];
				var ray = new Ray(from, (to - from).normalized);

				if (Physics.Raycast(ray, out RaycastHit hitInfo, 50) && hitInfo.collider.gameObject == _projection.gameObject)
				{
					Vector3 canvasHitPosition = _canvas.GetHitPositionByCoord(hitInfo.textureCoord);
					to = from + (canvasHitPosition - from).normalized * 50f;
					if (PerformRaycast(from, to, LayerMask.GetMask("UI", "UIHidden"), currentEventCamera, resultAppendList, hitInfo.point))
					{
						eventData.rayHitIndex = i;
						break;
					}
				}
			}
		}

		bool PerformRaycast(Vector3 from, Vector3 to, LayerMask layerMask, Camera currentEventCamera, List<RaycastResult> resultAppendList, Vector3 hitPosition)
		{
			var hitSomething = false;
			var rayDistance = Vector3.Distance(to, from);
			var ray = new Ray(from, (to - from).normalized * rayDistance);
			var hitDistance = rayDistance;

			m_RaycastResultsCache.Clear();
			SortedRaycastGraphics(_canvas, ray, hitDistance, layerMask, currentEventCamera, m_RaycastResultsCache);

			foreach (var hitData in m_RaycastResultsCache)
			{
				var validHit = true;

				var go = hitData.graphic.gameObject;
				if (m_IgnoreReversedGraphics)
				{
					var forward = ray.direction;
					var goDirection = go.transform.rotation * Vector3.forward;
					validHit = Vector3.Dot(forward, goDirection) > 0;
				}

				validHit &= hitData.distance < hitDistance;

				if (validHit)
				{
					var trans = go.transform;
					var transForward = trans.forward;
					var castResult = new RaycastResult
					{
						gameObject = go,
						module = this,
						distance = hitData.distance,
						index = resultAppendList.Count,
						depth = hitData.graphic.depth,
						sortingLayer = _canvas.sortingLayerID,
						sortingOrder = _canvas.sortingOrder,
						worldPosition = hitPosition,
						worldNormal = -transForward,
						screenPosition = hitData.screenPosition,
						displayIndex = hitData.displayIndex,
					};
					resultAppendList.Add(castResult);

					hitSomething = true;
				}
			}
			return hitSomething;
		}

		static void SortedRaycastGraphics(Canvas canvas, Ray ray, float maxDistance, LayerMask layerMask, Camera eventCamera, List<RaycastHitData> results)
		{
			var graphics = GraphicRegistry.GetGraphicsForCanvas(canvas);

			s_SortedGraphics.Clear();
			for (int i = 0; i < graphics.Count; ++i)
			{
				var graphic = graphics[i];

				// -1 means it hasn't been processed by the canvas, which means it isn't actually drawn
				if (graphic.depth == -1 || !graphic.raycastTarget || graphic.canvasRenderer.cull) continue;

				if (((1 << graphic.gameObject.layer) & layerMask) == 0) continue;

				var raycastPadding = graphic.raycastPadding;
				if (RayIntersectsRectTransform(graphic.rectTransform, raycastPadding, ray, out var worldPos, out var distance))
				{
					if (distance <= maxDistance)
					{
						Vector2 screenPos = eventCamera.WorldToScreenPoint(worldPos);
						// mask/image intersection - See Unity docs on eventAlphaThreshold for when this does anything
						if (graphic.Raycast(screenPos, eventCamera))
						{
							s_SortedGraphics.Add(new RaycastHitData(graphic, worldPos, screenPos, distance, eventCamera.targetDisplay));
						}
					}
				}
			}
			results.AddRange(s_SortedGraphics);
		}

		static bool RayIntersectsRectTransform(RectTransform transform, Vector4 raycastPadding, Ray ray, out Vector3 worldPosition, out float distance)
		{
			GetRectTransformWorldCorners(transform, raycastPadding, s_Corners);
			var plane = new Plane(s_Corners[0], s_Corners[1], s_Corners[2]);

			if (plane.Raycast(ray, out var enter))
			{
				var intersection = ray.GetPoint(enter);

				var bottomEdge = s_Corners[3] - s_Corners[0];
				var leftEdge = s_Corners[1] - s_Corners[0];
				var bottomDot = Vector3.Dot(intersection - s_Corners[0], bottomEdge);
				var leftDot = Vector3.Dot(intersection - s_Corners[0], leftEdge);

				// If the intersection is right of the left edge and above the bottom edge.
				if (leftDot >= 0f && bottomDot >= 0f)
				{
					var topEdge = s_Corners[1] - s_Corners[2];
					var rightEdge = s_Corners[3] - s_Corners[2];
					var topDot = Vector3.Dot(intersection - s_Corners[2], topEdge);
					var rightDot = Vector3.Dot(intersection - s_Corners[2], rightEdge);

					// If the intersection is left of the right edge, and below the top edge
					if (topDot >= 0f && rightDot >= 0f)
					{
						worldPosition = intersection;
						distance = enter;
						return true;
					}
				}
			}

			worldPosition = Vector3.zero;
			distance = 0f;
			return false;
		}

		static void GetRectTransformWorldCorners(RectTransform transform, Vector4 offset, Vector3[] fourCornersArray)
		{
			if (fourCornersArray == null || fourCornersArray.Length < 4)
			{
				Debug.LogError("Calling GetRectTransformWorldCorners with an array that is null or has less than 4 elements.");
				return;
			}
			var rect = transform.rect;
			var x0 = rect.x + offset.x;
			var y0 = rect.y + offset.y;
			var x1 = rect.xMax - offset.z;
			var y1 = rect.yMax - offset.w;
			fourCornersArray[0] = new Vector3(x0, y0, 0f);
			fourCornersArray[1] = new Vector3(x0, y1, 0f);
			fourCornersArray[2] = new Vector3(x1, y1, 0f);
			fourCornersArray[3] = new Vector3(x1, y0, 0f);

			var localToWorldMatrix = transform.localToWorldMatrix;
			for (var index = 0; index < 4; ++index)
				fourCornersArray[index] = localToWorldMatrix.MultiplyPoint(fourCornersArray[index]);
		}
	}
}