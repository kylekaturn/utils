using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.XR;

namespace Utils.XR
{
	public class Headset : TrackedPoseDriver
	{
		[Space(10)]
		public XRCamera xrCamera;
		public Action OnTransformUpdate;
		private InputAction _positionAction;
		private InputAction _rotationAction;

		protected override void PerformUpdate()
		{
			_positionAction = positionAction;
			_rotationAction = rotationAction;
			base.PerformUpdate();
			OnTransformUpdate?.Invoke();
		}

		public void PauseTracking()
		{
			positionAction = null;
			rotationAction = null;
		}

		public void ResumeTracking()
		{
			positionAction = _positionAction;
			rotationAction = _rotationAction;
		}
	}
}