using System;
using System.Collections.Generic;

namespace Utils.XR.Measures.Data
{
	[Serializable]
	public class MeasureData
	{
		public string name;
		public string version;
		public string device;
		public string unityVersion;
		public string graphicsAPI;
		public float eyeTextureWidth;
		public float eyeTextureHeight;
		public XRConfiguration renderConfiguration;
		public int blankGPUTime = 0;
		public List<MeasureResult> measureResults;
		public string comment;
		public string time;
	}
	
	[Serializable]
	public class MeasureResult
	{
		public string name = "";
		public int count = 1;
		public int triangles = 0;
		public int totalGPUTime = 0;
		public int selfGPUTime = 0;
	}
}