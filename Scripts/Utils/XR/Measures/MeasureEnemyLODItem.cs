using System.Collections.Generic;
using UnityEngine;

namespace Utils.XR.Measures
{
	public class MeasureEnemyLODItem : MeasureItem
	{
		//public EnemyLODGroup lodGroup;
		public List<SkinnedMeshRenderer> skinnedMeshRenderers;
		public List<Material> materials;

		public override void SetItem(int index)
		{
			//if (lodGroup.lodCount > 0) lodGroup.SetLOD((lodGroup.lodCount - 1) - (index % lodGroup.lodCount));
			//if (materials.Count > 0) skinnedMeshRenderers.ForEach(x => x.SetMaterial(materials[Mathf.FloorToInt((float) index / lodGroup.lodCount)]));
		}

		public override int GetDuplicateCount()
		{
			return 0;
		}

		public override int GetItemsCount()
		{
			return 0;
			//return Mathf.Max(1, lodGroup.lodCount) * Mathf.Max(1, materials.Count);
		}

		public override string GetDescription()
		{
			return "";
			//return skinnedMeshRenderers[lodGroup.currentLOD].sharedMaterial.name + "_LOD" + lodGroup.currentLOD;
		}

		public override int GetTrianglesCount()
		{
			return 0;
			//return skinnedMeshRenderers[lodGroup.currentLOD].sharedMesh.triangles.Length / 3;
		}
	}
}