using UnityEngine;

namespace Utils.XR.Measures
{
	public abstract class MeasureItem : MonoBehaviour
	{
		public virtual void Initialize()
		{
		}

		public virtual void ResetItem()
		{
			SetItem(0);
		}

		public abstract int GetItemsCount();
		public abstract int GetDuplicateCount();
		public abstract void SetItem(int index);
		public abstract int GetTrianglesCount();
		public abstract string GetDescription();
	}
}