using System.Collections.Generic;
using UnityEngine;
using Utils.Extensions;
using Mathf = UnityEngine.Mathf;

namespace Utils.XR.Measures
{
	public class MeasureLODGroupItem : MeasureItem
	{
		public LODGroup lodGroup;
		public List<SkinnedMeshRenderer> skinnedMeshRenderers;
		public List<Material> materials;

		public override void SetItem(int index)
		{
			if (lodGroup.lodCount > 0) lodGroup.ForceLOD((lodGroup.lodCount - 1) - (index % lodGroup.lodCount));
			if (materials.Count > 0) skinnedMeshRenderers.ForEach(x => x.SetMaterial(materials[Mathf.FloorToInt((float) index / lodGroup.lodCount)]));
		}

		public override int GetDuplicateCount()
		{
			return 0;
		}

		public override int GetItemsCount()
		{
			return Mathf.Max(1, lodGroup.lodCount) * Mathf.Max(1, materials.Count);
		}

		public override string GetDescription()
		{
			return skinnedMeshRenderers[lodGroup.GetCurrentLOD()].sharedMaterial.name + "_LOD" + lodGroup.GetCurrentLOD();
		}

		public override int GetTrianglesCount()
		{
			return skinnedMeshRenderers[lodGroup.GetCurrentLOD()].sharedMesh.triangles.Length / 3;
		}
	}
}