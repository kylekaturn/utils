using System.Collections.Generic;
using System.Linq;

namespace Utils.XR.Measures
{
	public class MeasureRenderSettingItem : MeasureItem
	{
		private List<RenderConfiguration> _renderConfigurations;
		private List<RenderConfiguration> renderConfigurations => _renderConfigurations ??= GetComponentsInChildren<RenderConfiguration>().ToList();

		private int _index = 0;

		public override void SetItem(int index)
		{
			_index = index;
			XRPlayer.Instance.Setup(renderConfigurations[index].xrConfiguration);
		}

		public override int GetDuplicateCount()
		{
			return 1;
		}

		public override int GetItemsCount()
		{
			return renderConfigurations.Count;
		}

		public override string GetDescription()
		{
			return renderConfigurations[_index].name;
		}

		public override int GetTrianglesCount()
		{
			return 0;
		}
	}
}