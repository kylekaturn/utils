using System.Collections.Generic;
using UnityEngine;

namespace Utils.XR.Measures
{
	public class MeasureStaticMeshItem : MeasureItem
	{
		public MeshFilter meshFilter;
		public MeshRenderer meshRenderer;
		public List<Mesh> meshes;
		public List<Material> materials;

		public override int GetItemsCount()
		{
			return Mathf.Max(1, meshes.Count) * Mathf.Max(1, materials.Count);
		}

		public override int GetDuplicateCount()
		{
			return 1;
		}

		public override void SetItem(int index)
		{
			if (meshes.Count > 0) meshFilter.sharedMesh = meshes[index % meshes.Count];
			if (materials.Count > 0) meshRenderer.material = materials[Mathf.FloorToInt((float) index / Mathf.Max(1, meshes.Count))];
		}

		public override string GetDescription()
		{
			return meshRenderer.sharedMaterial.name;
		}

		public override int GetTrianglesCount()
		{
			return meshFilter.sharedMesh.triangles.Length / 3;
		}
	}
}