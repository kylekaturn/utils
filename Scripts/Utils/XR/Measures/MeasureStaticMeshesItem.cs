using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Utils.XR.Measures
{
	public class MeasureStaticMeshesItem : MeasureItem
	{
		public List<MeshFilter> meshFilters;
		public List<MeshRenderer> meshRenderers;
		public List<Mesh> meshes;
		public List<Material> materials;

		public override int GetItemsCount()
		{
			return Mathf.Max(1, meshes.Count) * Mathf.Max(1, materials.Count);
		}

		public override void SetItem(int index)
		{
			if (meshes.Count > 0) meshFilters.ForEach(x => x.sharedMesh = meshes[index % meshes.Count]);
			if (materials.Count > 0) meshRenderers.ForEach(x => x.material = materials[Mathf.FloorToInt((float) index / Mathf.Max(1, meshes.Count))]);
		}

		public override string GetDescription()
		{
			return meshRenderers[0].sharedMaterial.name;
		}

		public override int GetDuplicateCount()
		{
			return 1;
		}

		public override int GetTrianglesCount()
		{
			return meshFilters.Sum(filter => filter.sharedMesh.triangles.Length / 3);
		}
	}
}