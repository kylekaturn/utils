using System;
using System.Reflection;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.XR;

namespace Utils.XR
{
	public class XRCamera : MonoBehaviour
	{
		public new Camera camera;
		public Volume postProcessVolume;
		
		private UniversalAdditionalCameraData _cameraData;
		public UniversalAdditionalCameraData cameraData
		{
			get
			{
				if (!_cameraData) _cameraData = camera.GetComponent<UniversalAdditionalCameraData>();
				return _cameraData;
			}
		}

		private UniversalRenderPipelineAsset _renderPipelineAsset;
		public UniversalRenderPipelineAsset renderPipelineAsset
		{
			get
			{
				if (!_renderPipelineAsset) _renderPipelineAsset = GraphicsSettings.renderPipelineAsset as UniversalRenderPipelineAsset;
				return _renderPipelineAsset;
			}
		}

		private UniversalRendererData _universalRendererData;
		public UniversalRendererData universalRendererData
		{
			get
			{
				if (!_universalRendererData)
				{
					FieldInfo propertyInfo = renderPipelineAsset.GetType().GetField("m_RendererDataList", BindingFlags.Instance | BindingFlags.NonPublic);
					_universalRendererData = ((ScriptableRendererData[]) propertyInfo?.GetValue(renderPipelineAsset))[0] as UniversalRendererData;
				}
				return _universalRendererData;
			}
		}

		public Bloom bloomComponent
		{
			get => postProcessVolume.profile.components[0] as Bloom;
		}

		public RenderObjects renderObjects
		{
			get => universalRendererData.rendererFeatures[0] as RenderObjects;
		}

		public RenderingMode renderingMode
		{
			get => universalRendererData.renderingMode;
			set
			{
				if (universalRendererData.renderingMode == value) return;
				universalRendererData.renderingMode = value;
				universalRendererData.SetDirty();
			}
		}

		public DepthPrimingMode depthPrimingMode
		{
			get => universalRendererData.depthPrimingMode;
			set
			{
				if (universalRendererData.depthPrimingMode == value) return;
				universalRendererData.depthPrimingMode = value;
				universalRendererData.SetDirty();
			}
		}

		public CopyDepthMode copyDepthMode
		{
			get => universalRendererData.copyDepthMode;
			set
			{
				if (universalRendererData.copyDepthMode == value) return;
				universalRendererData.copyDepthMode = value;
				universalRendererData.SetDirty();
			}
		}

		public bool useNativeRenderpass
		{
			get => universalRendererData.useNativeRenderPass;
			set
			{
				if (universalRendererData.useNativeRenderPass == value) return;
				universalRendererData.useNativeRenderPass = value;
				universalRendererData.SetDirty();
			}
		}

		public IntermediateTextureMode intermediateTextureMode
		{
			get => universalRendererData.intermediateTextureMode;
			set => universalRendererData.intermediateTextureMode = value;
		}

		public bool rendererFeature
		{
			get => universalRendererData.rendererFeatures[0].isActive;
			set => universalRendererData.rendererFeatures.ForEach(x => x.SetActive(value));
		}

		public bool depthTexture
		{
			get => cameraData.requiresDepthTexture;
			set => cameraData.requiresDepthTexture = value;
		}

		public bool opaqueTexture
		{
			get => cameraData.requiresColorTexture;
			set => cameraData.requiresColorTexture = value;
		}

		public bool shadow
		{
			get => cameraData.renderShadows;
			set => cameraData.renderShadows = value;
		}

		public bool skybox
		{
			get => camera.clearFlags == CameraClearFlags.Skybox;
			set => camera.clearFlags = value ? CameraClearFlags.Skybox : CameraClearFlags.SolidColor;
		}

		public bool postProcessing
		{
			get => cameraData.renderPostProcessing;
			set => cameraData.renderPostProcessing = value;
		}

		public bool hdr
		{
			get => camera.allowHDR;
			set => camera.allowHDR = value;
		}

		public bool bloom
		{
			get => bloomComponent.active;
			set => bloomComponent.active = value;
		}

		public float bloomThreshold
		{
			get => bloomComponent.threshold.value;
			set => bloomComponent.threshold.value = value;
		}

		public float bloomItensity
		{
			get => bloomComponent.intensity.value;
			set => bloomComponent.intensity.value = value;
		}

		public float bloomScatter
		{
			get => bloomComponent.scatter.value;
			set => bloomComponent.scatter.value = value;
		}

		public int bloomMaxIteration
		{
#if UNITY_2023_1_OR_NEWER
			get => bloomComponent.maxIterations.value;
			set => bloomComponent.maxIterations.value = value;
#else
			get => bloomComponent.skipIterations.value;
			set => bloomComponent.skipIterations.value = value;
#endif
		}

		private AntialiasingType _antialiasingType;

		public AntialiasingType antialiasingType
		{
			get => _antialiasingType;
			set
			{
				_antialiasingType = value;
				switch (value)
				{
					case AntialiasingType.Off:
						renderPipelineAsset.msaaSampleCount = 1;
						XRDevice.UpdateEyeTextureMSAASetting();
						cameraData.antialiasing = AntialiasingMode.None;
						break;

					case AntialiasingType.MSAA:
						cameraData.antialiasing = AntialiasingMode.None;
						msaa = _msaa;
						break;

					case AntialiasingType.FXAA:
						renderPipelineAsset.msaaSampleCount = 1;
						XRDevice.UpdateEyeTextureMSAASetting();
						cameraData.antialiasing = AntialiasingMode.FastApproximateAntialiasing;
						break;

					case AntialiasingType.SMAA:
						renderPipelineAsset.msaaSampleCount = 1;
						XRDevice.UpdateEyeTextureMSAASetting();
						cameraData.antialiasing = AntialiasingMode.SubpixelMorphologicalAntiAliasing;
						break;
				}
			}
		}

		private MsaaQuality _msaa;

		public MsaaQuality msaa
		{
			get => _msaa;
			set
			{
				_msaa = value;
				if (antialiasingType != AntialiasingType.MSAA) return;
				if (renderPipelineAsset.msaaSampleCount != (int) value)
				{
					renderPipelineAsset.msaaSampleCount = (int) value;
					XRDevice.UpdateEyeTextureMSAASetting();
				}
			}
		}

		public float renderScale
		{
			get => renderPipelineAsset.renderScale;
			set => renderPipelineAsset.renderScale = Mathf.Clamp(value, 0.5f, 2.0f);
		}
	}
}