using UnityEngine;

namespace Utils.XR
{
	public class XRCanvas : MonoBehaviour
	{
		private Canvas _canvas;

		private void Awake()
		{
			_canvas = GetComponent<Canvas>();
		}

		private void Start()
		{
			if (XRPlayer.Instance != null) _canvas.worldCamera = XRPlayer.Instance.headset.xrCamera.camera;
		}
	}
}