using System;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.Rendering.Universal;
using Utils.Attributes;

namespace Utils.XR
{
	[Serializable]
	public class XRConfiguration
	{
		[Header("Translation")]
		public bool translateEnabled = true;
		public float translationSpeed = 1f;
		public bool turnEnabled = true;
		public float turnAngle = 45f;

		[Header("UI")]
		public bool showQuestController = true;
		public bool uiInteractorEnabled = true;
		public bool autoSwitchUIInteractor = false;

		[Header("Rendering")]
		public RenderingMode renderingMode = RenderingMode.Forward;
		public DepthPrimingMode depthPrimingMode = DepthPrimingMode.Disabled;
		public CopyDepthMode copyDepthMode = CopyDepthMode.AfterOpaques;
		public bool useNativeRenderpass = false;
		public IntermediateTextureMode intermediateTextureMode = IntermediateTextureMode.Auto;
		public bool rendererFeature = false;
		public bool depthTexture = false;
		public bool opaqueTextue = false;
		public bool shadow = true;
		public bool skybox = true;
		public bool postProcessing = true;
		[Label("HDR")] public bool hdr = true;
		public bool bloom = true;
		[Conditional(nameof(bloom))]
		[Range(0f, 3f)] public float bloomThreshold = 0.9f;
		[Conditional(nameof(bloom))]
		[Range(1f, 20f)] public float bloomIntensity = 6f;
		[Conditional(nameof(bloom))]
		[Range(0f, 1f)] public float bloomScatter = 0.5f;
		[Conditional(nameof(bloom))]
		[Range(1, 16)] public int bloomMaxIteration = 6;
		[Label("Anti-aliasing Type")] public AntialiasingType antialiasingType = AntialiasingType.MSAA;
		[Label("MSAA")] public MsaaQuality msaa = MsaaQuality._2x;
		[Range(0.5f, 2f)] public float renderScale = 1f;
		[Label("FFR")] public FFRLevel ffrLevel = FFRLevel.Off;
		[Label("CPU Level")] public ProcessorPerformanceLevel cpuLevel = ProcessorPerformanceLevel.SustainedHigh;
		[Label("GPU Level")] public ProcessorPerformanceLevel gpuLevel = ProcessorPerformanceLevel.SustainedHigh;

		[Header("Input Simulator")]
		public float simualatorHeight = 1.5f;
	}
	
	public enum ProcessorPerformanceLevel
	{
		PowerSavings = 0,
		SustainedLow = 1,
		SustainedHigh = 2,
		Boost = 3,
	}

	public enum AntialiasingType
	{
		Off = 0,
		MSAA = 1,
		FXAA = 2,
		SMAA = 3
	}

	public enum FFRLevel
	{
		Off = 0,
		Low = 1,
		Med = 2,
		High = 3,
		Top = 4
	}

	public enum PerformanceLevel
	{
		Lowest = 0,
		Low = 1,
		Med = 2,
		High = 3,
		Highest = 4
	}
}

#if UNITY_EDITOR
namespace Utils.XR
{
	using UnityEditor;

	[CustomPropertyDrawer(typeof(XRConfiguration))]
	public class XRPlayerConfigDrawer : PropertyDrawer
	{
		private float height = 0;

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			var fieldInfos = typeof(XRConfiguration).GetFields();
			var propertyObject = property.serializedObject.GetIterator();
			height = 40;
			while (propertyObject.NextVisible(true))
			{
				if (propertyObject.depth == 0) continue;
				bool conditionalEnabled = true;
				foreach (var field in fieldInfos)
				{
					if (field.Name == propertyObject.name)
					{
						if (Attribute.IsDefined(field, typeof(ConditionalAttribute)))
						{
							var conditionalAttribute = field.GetCustomAttributes<ConditionalAttribute>().First();
							conditionalEnabled = ConditionalAttribute.GetConditionalHideAttributeResult(conditionalAttribute, propertyObject);
						}
					}
				}

				if (!conditionalEnabled) continue;
				height += EditorGUI.GetPropertyHeight(propertyObject);
				height += 2;
			}
			return height;
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			var fieldInfos = typeof(XRConfiguration).GetFields();
			var propertyObject = property.serializedObject.GetIterator();

			propertyObject.Next(true);
			propertyObject.NextVisible(true);
			propertyObject.NextVisible(true);

			var xPos = position.x + 10;
			var width = position.width - 10;

			while (propertyObject.NextVisible(true))
			{
				if (propertyObject.depth == 0) continue;

				bool conditionalEnabled = true;
				foreach (var field in fieldInfos)
				{
					if (field.Name == propertyObject.name)
					{
						if (Attribute.IsDefined(field, typeof(ConditionalAttribute)))
						{
							var conditionalAttribute = field.GetCustomAttributes<ConditionalAttribute>().First();
							conditionalEnabled = ConditionalAttribute.GetConditionalHideAttributeResult(conditionalAttribute, propertyObject);
						}
					}
				}

				if (!conditionalEnabled) continue;

				position.x = xPos - 10;
				position.width = width + 10;
				position.height = EditorGUI.GetPropertyHeight(propertyObject, true);
				EditorGUI.PropertyField(position, propertyObject, false);
				position.y += position.height + 4;
				height += EditorGUI.GetPropertyHeight(propertyObject);
				height += 2;
			}
		}
	}
}
#endif