using UnityEngine;

namespace Utils.XR
{
	[ExecuteAlways]
	public class XRConfigurationApplier : MonoBehaviour
	{
		private XRPlayer _xrPlayer;
		private XRCamera _xrCamera;
		private XRConfiguration _xrConfiguration;

		private void Awake()
		{
			if (Application.isPlaying) Destroy(this);
		}

		private void OnEnable()
		{
			_xrPlayer = GetComponent<XRPlayer>();
			if (_xrPlayer != null)
			{
				_xrCamera = _xrPlayer.headset.xrCamera;
				_xrConfiguration = _xrPlayer.xrConfiguration;
			}
		}

		private void Update()
		{
			if (Application.isPlaying) return;
			_xrCamera.renderingMode = _xrConfiguration.renderingMode;
			_xrCamera.depthPrimingMode = _xrConfiguration.depthPrimingMode;
			_xrCamera.copyDepthMode = _xrConfiguration.copyDepthMode;
			_xrCamera.useNativeRenderpass = _xrConfiguration.useNativeRenderpass;
			_xrCamera.intermediateTextureMode = _xrConfiguration.intermediateTextureMode;
			_xrCamera.rendererFeature = _xrConfiguration.rendererFeature;
			_xrCamera.depthTexture = _xrConfiguration.depthTexture;
			_xrCamera.opaqueTexture = _xrConfiguration.opaqueTextue;
			_xrCamera.shadow = _xrConfiguration.shadow;
			_xrCamera.skybox = _xrConfiguration.skybox;
			_xrCamera.postProcessing = _xrConfiguration.postProcessing;
			_xrCamera.hdr = _xrConfiguration.hdr;
			_xrCamera.bloom = _xrConfiguration.bloom;
			_xrCamera.bloomThreshold = _xrConfiguration.bloomThreshold;
			_xrCamera.bloomItensity = _xrConfiguration.bloomIntensity;
			_xrCamera.bloomScatter = _xrConfiguration.bloomScatter;
			_xrCamera.bloomMaxIteration = _xrConfiguration.bloomMaxIteration;
			_xrCamera.antialiasingType = _xrConfiguration.antialiasingType;
			_xrCamera.msaa = _xrConfiguration.msaa;
			_xrCamera.renderScale = _xrConfiguration.renderScale;

			//아래 파라미터는 에디터에서 실시간으로 업데이트될 필요성이 적어서 제외함
			//xrPlayer.showQuestController = xrConfiguration.showQuestController;
			//xrPlayer.ffrLevel = xrConfiguration.ffrLevel;
			//xrPlayer.cpuLevel = xrConfiguration.cpuLevel;
			//xrPlayer.gpuLevel = xrConfiguration.gpuLevel;
		}
	}
}