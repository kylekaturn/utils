using UnityEngine;

namespace Utils.XR
{
	public enum XRPlatform
	{
		Editor,
		OculusLink,
		Device
	}
}